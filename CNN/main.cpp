#include <iostream>
#include "list"
#include "vector"
#include "cmath"
#include "set"
#include "mysql/include/jdbc/cppconn/connection.h"

#include "boost/lexical_cast.hpp"
#include "Neuron.h"
#include "NeuronLayer.h"
#include "NeuronUtils.h"
#include "NeuronOps.h"
#include "fstream"
#include "LogIntoDb.h"
#include "Data.h"
#include "Training.h"
#include "TestData.h"
#include "Output.h"

using namespace std;
using namespace sql;



template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

vector<Training> readData(char* FILENAME,int channel,bool readBoundingBoxFile)
{
    vector<Data> neuronInputData;

    vector<Training> neuronInputDataT;

    FILE* fp = fopen(FILENAME, "r");
    FILE* fpExpectedData = fopen("/home/user/CLionProjects/CNN/training/expectedOutput", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    char* line = NULL;
    size_t len = 0;
    vector<string> originInputData;
    while ((getline(&line, &len, fp)) != -1) {
        // using printf() in all tests for consistency
        originInputData.push_back(line);
    }
    fclose(fp);

    ///Read Bounding Box file
    char* BOUNDING_BOX_FILE="/home/user/CLionProjects/CNN/training/boxes";
    FILE* boudingBoxFile = fopen(BOUNDING_BOX_FILE, "r");
    if (boudingBoxFile  == NULL)
        exit(EXIT_FAILURE);

    line = NULL;
    len = 0;
    vector<vector<float>> boundingBoxData;

    while ((getline(&line, &len, fp)) != -1) {
        // using printf() in all tests for consistency
        vector<string> tempBox=split(line,',');
        vector<float > box;
        for(string b:tempBox)
        {
            box.push_back(stof(b));
        }
        boundingBoxData.push_back(box);
    }
    fclose(boudingBoxFile);
    ///End of Read Bounding Box file

    vector<float> expectedDataVect;
    while ((getline(&line, &len, fpExpectedData)) != -1) {
        // using printf() in all tests for consistency
        expectedDataVect.push_back(atof(line));
    }
    fclose(fpExpectedData);

    int k=-1;
    for(string line:originInputData)
    {
        k++;
        Training t;
        t.setExpectedOutput(expectedDataVect[k]);
        if(boundingBoxData.size() > 0)
        {
            t.setBoundingBoxVect(boundingBoxData.at(k));
        }
        vector<string>input=split(line,',');
        vector<Data> dataVect;
        for(int n=0;n<input.size();n+=channel)
        {
            vector<float> inputD;
            Data d;
            for(int m=n;m<(n+channel);m++)
            {
                string s=input[m];
                float v=::atof(s.c_str());
                inputD.push_back(v);
            }
            d.setData(inputD);
            dataVect.push_back(d);
        }
        t.setData(dataVect);
        neuronInputDataT.push_back(t);
    }
    return neuronInputDataT;
}

vector<TestData> readTestData(char* FILENAME,int channel)
{
    vector<Data> neuronInputData;

    vector<TestData> neuronInputDataT;

    FILE* fp = fopen(FILENAME, "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    char* line = NULL;
    size_t len = 0;

    vector<string> originInputData;
    while ((getline(&line, &len, fp)) != -1) {
        // using printf() in all tests for consistency
        originInputData.push_back(line);
    }
    fclose(fp);

    int k=-1;
    for(string line:originInputData)
    {
        k++;
        TestData t;
        vector<string>input=split(line,',');
        vector<Data> dataVect;
        for(int n=0;n<input.size();n+=channel)
        {
            vector<float> inputD;
            Data d;
            for(int m=n;m<(n+channel);m++)
            {
                string s=input[m];
                float v=::atof(s.c_str());
                inputD.push_back(v);
            }
            d.setData(inputD);
            dataVect.push_back(d);
        }
        t.setTestData(dataVect);
        neuronInputDataT.push_back(t);
    }
    return neuronInputDataT;
}

void connect(NeuronLayer* layer1,NeuronLayer* layer2)
{
//    layer1->setNextLayer(layer2);
//    layer2->setPrevLayer(layer1);
//
//    if(layer1->getMaxPoolLayer()!=NULL)
//    {
//        NeuronLayer* maxPoolLayer=layer1->getMaxPoolLayer();
//        maxPoolLayer->setNextLayer(layer2);
//        layer2->setPrevLayer(maxPoolLayer);
//    }
}

MLDb *mlDb=new MLDb();
vector<float> getExpectedOutput(int label);
void resetNetwork(SuperNeuronLayer* layer);
void printLayerLink(NeuronLayer* layer);

void maxPoolLayer(NeuronLayer* layer,int fw,int fh,int stride)
{
    MaxPool* maxPool=new MaxPool(fw,fh,stride,layer->getChannel());
    setMaxPool(maxPool,layer);
    layer->setMaxPool(maxPool);
//    layer->getMaxPoolLayer()->setPrevLayer(layer->getPrevLayer()); //Max pool previous handling
}
void printNeuronLayer(NeuronLayer *layer,int superLayerId,int iteration){
        NeuronVect*** neuronVects=layer->getLayer();
        int layerId=layer->getId();
        for(int r=layer->getRowStartIndex();r<layer->getRowEndIndex();r++) {
            for (int c = layer->getColStartIndex(); c < layer->getColEndIndex(); c++) {
                vector<Neuron *> *neuronVect = neuronVects[r][c]->getNeuron();
                for (int ch = 0; ch < layer->getChannel(); ch++) {
                    Neuron *n = neuronVect->at(ch);
                    int neuronId=n->getId();
                    mlDb->insertNeuron(superLayerId,layerId,neuronId,ch,iteration);
                    vector<Link *> *frontLinks = n->getAllFrontLinks();
                    for (int l = 0; l < frontLinks->size(); l++) {
                        Link *link = frontLinks->at(l);
                        int linkId = link->getId();
                        int weightId = link->getWeight()->getId();
                        float wtValue = link->getWeight()->getWeight();
                        float gradient= link->getWeight()->getGradient();

//                        weightId = mlDb->fetchWeightId(weightId, wtValue);
                         mlDb->insertWeight(weightId,wtValue,iteration,gradient);
                        int toNeuronId=link->getToNeuron()->getId();
                        //weightId,linkId,fromNeuronId,toNeuronId
                        float linkGradient=link->getGradient();
                        mlDb->insertLink(weightId,linkId,neuronId,toNeuronId, true,iteration,linkGradient);
                    }


                    ////BackLink///
                    vector<Link *> *backLinks = n->getAllBackLinks();
                    for (int l = 0; l < backLinks->size(); l++) {
                        Link *link = backLinks->at(l);
                        int linkId = link->getId();
                        int weightId = link->getWeight()->getId();
                        float wtValue = link->getWeight()->getWeight();
                        float gradient= link->getWeight()->getGradient();
//                        weightId = mlDb->fetchWeightId(weightId, wtValue);
                        mlDb->insertWeight(weightId,wtValue,iteration,gradient);
                        int fromNeuronId=link->getFromNeuron()->getId();
                        float linkGradient=link->getGradient();
                        //weightId,linkId,fromNeuronId,toNeuronId
                        mlDb->insertLink(weightId,linkId,neuronId,fromNeuronId, false,
                                iteration,gradient);
                    }
                    vector<NeuronData *> *neuronDataVect = n->getData();
                    for (NeuronData *nd:*neuronDataVect) {
                        float input = nd->getInput();
                        float output = nd->getOutput();
                        string activation = n->getActivation();
                        float gradient=nd->getGradient();
                        mlDb->insertND(n->getId(), nd->getId(), input, output,iteration,gradient);
                    }
                }
            }
        }
}

void printNetwork(SuperNeuronLayer *superLayer,int x);
void executeQuery();
void cleanData();

int main() {
    //hyper parameter settings
    string sigmoidAct = "sigmoid";
    string tanhAct = "tanh";
    string softmaxAct = "softmax";
    string reluAct = "relu";
    string linearAct="linear";

    float learningRate = 0.5;
    //input layer settings
    int row = 3, column = 3;
    //end of hyper parameter settings

    int channel = 1;

    vector<Training> neuronInputData = readData("/home/user/CLionProjects/CNN/training/dst.csv",
            channel, true);
    //start of data preparataion
    NeuronVect ***inputNeuronVects = createNeurons(row, column, channel, "","inputLayer");

    SuperNeuronLayer* superLayer1=new SuperNeuronLayer();
    NeuronLayer *layer1 = createNeuronLayer(inputNeuronVects, row, column, channel,superLayer1);
    layer1->setChannel(channel);
    layer1->setNote("layer1");
    superLayer1->setPrevSuperLayer(NULL);


    vector<Filter*>*filterVect=createFilter(1,1,20, layer1);
    SuperNeuronLayer* superLayer2=new SuperNeuronLayer();
    NeuronLayer *layer2 = convolve(layer1, reluAct,1, false,superLayer2,filterVect);
    layer2->setNote("layer2");
    connectSuperLayer(superLayer1,superLayer2);

    /////Dense layer as a FC /////////////
//    NeuronLayer * denseLayer = flatten(layer2,layer1);
//    denseLayer->setFilterVect(layer2->getFilterVect());
//    denseLayer->setNote("dense");
//    superLayer2->getNeuronLayer()->clear();
//    superLayer2->getNeuronLayer()->push_back(denseLayer);
    /////End of Dense layer as a FC /////////////


        /////Output layer as a FC /////////////
//    vector<Filter*>*filterVect3=createFCFilter(denseLayer->getRow(),OUTPUT_NEURONS, denseLayer);
//    SuperNeuronLayer* superLayer3=new SuperNeuronLayer();
//    NeuronLayer *outputLayer = dense(denseLayer, softmaxAct,superLayer3,filterVect3);
//    outputLayer->setOutputLayer(true);
//    connectSuperLayer(superLayer2,superLayer3);
//    superLayer3->setNextSuperLayer(NULL);
    /////End of Output layer as a FC /////////////
    vector<Filter*>*filterVect3=createFilter(layer2->getRow(),layer2->getColumn(),OUTPUT_NEURONS, layer2);
    SuperNeuronLayer* superLayer3=new SuperNeuronLayer();
    NeuronLayer *outputLayer = convolveDense(layer2, softmaxAct,1,filterVect3,superLayer3,filterVect3);
    outputLayer->setOutputLayer(true);
    connectSuperLayer(superLayer2,superLayer3);
    superLayer3->setNextSuperLayer(NULL);

//
////    vector<Filter*>*filterB=createFCFilter(denseLayer->getRow(),20, denseLayer);
////    NeuronLayer *outputLayerE = dense(denseLayer, reluAct,superLayer2,filterB);
//    ////Output 1 ////
//    vector<Filter*>*filterC=createFCFilter(denseLayer->getRow(),100, denseLayer);
//    NeuronLayer *outputLayerE = dense(denseLayer, reluAct,superLayer2,filterC);
//    outputLayerE->setNote("branch1");
//
//
//    vector<Filter*>*filterC1=createFCFilter(outputLayerE->getRow(),100, outputLayerE);
//    NeuronLayer *interLayer1 = dense(outputLayerE, reluAct,superLayer2,filterC1);
//    interLayer1->setNote("branch1");
//
//
//    vector<Filter*>*filterD=createFCFilter(interLayer1->getRow(),100, interLayer1);
//    NeuronLayer *outputLayerE1 = dense(interLayer1, reluAct,superLayer2,filterD);
//    outputLayerE1->setNote("branch2");
////    outputLayerE1->setOutputLayer(true);
//    ////End of Output 1 ////
//
//
//    vector<Filter*>*filterE=createFCFilter(outputLayerE1->getRow(),1, outputLayerE1);
//    NeuronLayer *outputLayerE2 = dense(outputLayerE1, linearAct,superLayer2,filterE);
//    outputLayerE2->setNote("branch3");
//    outputLayerE2->setOutputLayer(true);
//
//    //////////Output 2 //////
//    vector<Filter*>*filterC2=createFCFilter(outputLayerE->getRow(),100, outputLayerE);
//    NeuronLayer *interLayer2 = dense(outputLayerE, reluAct,superLayer2,filterC2);
//    interLayer1->setNote("branch1");
//
//    vector<Filter*>*filterD2=createFCFilter(interLayer2->getRow(),100, interLayer2);
//    NeuronLayer *outputLayerE12 = dense(interLayer2, reluAct,superLayer2,filterD2);
//    outputLayerE1->setNote("branch2");
//
//    //////////End of Output 2 //////
//
//    vector<Filter*>*filterF=createFCFilter(outputLayerE12->getRow(),1, outputLayerE12);
//    NeuronLayer *outputLayerE3 = dense(outputLayerE12, linearAct,superLayer2,filterF);
//    outputLayerE3->setNote("branch4");
//    outputLayerE3->setOutputLayer(true);


//---------------------TBU-----------------------
//    vector<Filter*>*filterc=createFCFilter(denseLayer->getRow(),1, denseLayer);
//    NeuronLayer *outputLayerE1 = dense(denseLayer, linearAct,superLayer2,filterc);
//    outputLayerE1->setOutputLayer(true);
//    printLayerLink(denseLayer);
//---------------------ENd TBU-----------------------


//    cleanData();
//    printNetwork(superLayer1,0);
//    executeQuery();

    long start = time(0);
    int iterations =200;
    Output output;
    for (int x = 0; x < iterations; x++) {
        cout<<"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX->"<<x<<endl;
        vector<float> totalErrr;
        for(int s=0;s<neuronInputData.size();s+=BATCH_SIZE) {
            vector<vector<float>> expectedOutputs;
            vector<vector<float>> boundingBoxVects;


            for(int b=s,actualBatch=0;b<(s+BATCH_SIZE);b++,actualBatch++) {
                Training t=neuronInputData[b];
                vector<Data> inputDataFloat = t.getData();

                int z = 0;
                for (int i = 1; i < 4; i++) {
                    for (int j = 1; j < 4; j++) {
                        vector<Neuron *> *neuronVect = inputNeuronVects[i][j]->getNeuron();
                        vector<float> d=inputDataFloat[z].getData();
                        for (int c = 0; c < layer1->getChannel(); c++) {
                            Neuron *neuron = neuronVect->at(c);
                            NeuronData *data = neuron->getData()->at(actualBatch);
                            data->setInput(d[c]);
                            data->setOutput(d[c]);
                        }
                        z++;
                    }
                }
                vector<float> expectedOutput;
                int label = t.getExpectedOutput();
                expectedOutput = getExpectedOutput(label);
                ///Add bounding boxes in a expected output vector //
                for(float b:t.getBoundingBoxVect()){
                    boundingBoxVects.push_back(t.getBoundingBoxVect());
//                    esizexpectedOutput.push_back(b);
                }
                ///End of Add bounding boxes in a expected output vector //
                expectedOutputs.push_back(expectedOutput);
            }
            Output* outputE=new Output();
            outputE->setExpectedOutput(expectedOutputs);
            outputLayer->setExpectedOutput(outputE);


//            vector<vector<float>> expectedOutputs1;
//            vector<float> a;
//            a.push_back(0.5);
//            expectedOutputs1.push_back(a);
//            Output* outputE1=new Output();
//            outputE1->setExpectedOutput(expectedOutputs1);
//            outputLayerE2->setExpectedOutput(outputE1);
//
//
//            vector<vector<float>> expectedOutputs2;
//            vector<float> b;
//            b.push_back(0.7);
//            expectedOutputs2.push_back(b);
//            Output* outputE2=new Output();
//            outputE2->setExpectedOutput(expectedOutputs2);
//            outputLayerE3->setExpectedOutput(outputE2);



//            vector<vector<float>> expectedOutputs2;
//            vector<float> b;
//            b.push_back(0.5);
//            expectedOutputs2.push_back(b);
//            Output* outputE2=new Output();
//            outputE2->setExpectedOutput(expectedOutputs2);
//            outputLayerE1->setExpectedOutput(outputE2);


            forwardPropDev(superLayer1);
//            printLayerLink(layer6);
            float e=backPropDev(superLayer3);
            totalErrr.push_back(e);
            optmizeErrorDev(superLayer1, learningRate);
//            cout<<"outputLayerE1 "<<outputLayerE1->getBiasNeuronVect()[0][0]->getGradientWithMomentum()<<endl;
//            cout<<"outputLayerE "<<outputLayerE->getBiasNeuronVect()[0][0]->getGradientWithMomentum()<<endl;
//            cout<<"outputLayerE "<<outputLayer->getBiasNeuronVect()->at(0)->getGradientWithMomentum()<<endl;

//            printNetwork(superLayer1,x);
//            executeQuery();
//            optimizeBiasLinear(outputLayer,learningRate);
            resetNetwork(superLayer1);
        }
        float err=sumVector(totalErrr);
    }
    long end = time(0);
    cout<<"Execution time is "<<(end-start)<<endl;

//   ////Test the model /////
//    vector<TestData> testDataVect=readTestData("/home/user/CLionProjects/CNN/training/test.csv",channel);
//    TestData d1 = testDataVect[0];
//    vector<Data> testData=d1.getTestData();
//    int z = 0;
//    for (int i = 1; i < 29; i++) {
//        for (int j = 1; j < 29; j++) {
//            vector<Neuron *> *neuronVect = inputNeuronVects[i][j]->getNeuron();
//            vector<float> d=testData.at(z).getData();
//            for (int c = 0; c < layer1->getChannel(); c++) {
//                Neuron *neuron = neuronVect->at(c);
//                NeuronData *data = neuron->getData()->at(0);
//                data->setInput(d[c]);
//                data->setOutput(d[c]);
//            }
//            z++;
//        }
//    }
//    forwardPropDev(layer1);
    return 0;
}

void printNetwork(SuperNeuronLayer *superLayer,int iteration){
    while (superLayer!=NULL) {
        int superLayerId=superLayer->getId();
        vector<NeuronLayer *> *nextNeurons = superLayer->getNeuronLayer();
        for (NeuronLayer *layer:*nextNeurons) {
//            int superLayerMappingId=mlDb->fetchSuperLayerId(superLayerId,layer->getId());
            printNeuronLayer(layer,superLayerId,iteration);
        }
        superLayer=superLayer->getNextSuperLayer();
    }
}

void cleanData(){
    mlDb->deleteData();
}
void executeQuery(){

    mlDb->executeQuery();
}
void printLayerLink(NeuronLayer* layer)
{
    for(int r=layer->getRowStartIndex();r<layer->getRowEndIndex();r++)
    {
        for(int c=layer->getColStartIndex();c<layer->getColEndIndex();c++)
        {
            vector<Neuron*>* neuronVect=layer->getLayer()[r][c]->getNeuron();
            for(int ch=0;ch<layer->getChannel();ch++)
            {
                Neuron*neuron=neuronVect->at(ch);
                cout<<neuron->getFrontLinks()->size()<<endl;
                cout<<neuron->getBackLinks()->size()<<endl;

//                vector<NeuronData*>* nd=neuron->getData();
//                for(NeuronData*n:*nd)
//                {
//                    cout<<n->getOutput()<<",";
//                }
                cout<<endl;
            }
        }
    }
}


void removeVolatileLinks(NeuronLayer* layer){
    //Remove Volatile links
    NeuronVect***nvect=layer->getLayer();

    for(int r=0;r<layer->getRow();r++)
    {
        for(int c=0;c<layer->getColumn();c++)
        {
            vector<Neuron*>*neuronVect= nvect[r][c]->getNeuron();
            for(int ch=0;ch<layer->getChannel();ch++)
            {
                Neuron*n=neuronVect->at(ch);
                vector<Link*>* frontLinks=n->getAllFrontLinks();
                for(int l=0;l<frontLinks->size();l++)
                {
                    Link* link=frontLinks->at(l);
                    if(link->isVolatileF()== true)
                    {
                        frontLinks->erase(frontLinks->begin()+l);
                    }
                }
            }
        }
    }

    layer=layer->getMaxPoolLayer();
    if(layer!=NULL) {
        nvect = layer->getLayer();

        for (int r = 0; r < layer->getRow(); r++) {
            for (int c = 0; c < layer->getColumn(); c++) {
                vector<Neuron *> *neuronVect = nvect[r][c]->getNeuron();
                for (int ch = 0; ch < layer->getChannel(); ch++) {
                    Neuron *n = neuronVect->at(ch);
                    vector<Link *> *frontLinks = n->getAllFrontLinks();
                    for (int l = 0; l < frontLinks->size(); l++) {
                        Link *link = frontLinks->at(l);
                        if (link->isVolatileF() == true) {
                            frontLinks->erase(frontLinks->begin() + l);
                        }
                    }
                }
            }
        }
    }
}

void resetNetwork(SuperNeuronLayer* superLayer)
{
    while (superLayer!=NULL) {
        vector<NeuronLayer *> *nextNeurons = superLayer->getNeuronLayer();
        for (NeuronLayer *layer:*nextNeurons) {
                setBackLinkStatus(layer, true, true);
                setFrontLinkStatus(layer, true, true);
                removeVolatileLinks(layer);
        }
        superLayer=superLayer->getNextSuperLayer();
    }
}

vector<float> getExpectedOutput(int label)
{
    vector<float> expectedOutput;
    for(int i=0;i<OUTPUT_NEURONS;i++)
    {
        expectedOutput.push_back(0);
    }
    expectedOutput[label]=1;
    return expectedOutput;
}
