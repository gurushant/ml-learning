//
// Created by gurushant on 21/08/18.
//

#ifndef CNN_NEURONDATA_H
#define CNN_NEURONDATA_H
class NeuronData
{
public:NeuronData(int s)
    {
        statId++;
        setId(statId);
    }
public:
    float getInput() const {
        return input;
    }

    void setInput(float input) {
        NeuronData::input = input;
    }

    float getOutput() const {
        return output;
    }

    void setOutput(float output) {
        NeuronData::output = output;
    }

    float getGradient() const {
        return gradient;
    }

    void setGradient(float gradient) {
        NeuronData::gradient = gradient;
    }

private:
    float input=0;
    float output=0;
    float gradient=0;
    int id;
public:
    int getId() const;

    void setId(int id);

private:
    static int statId;
};
int NeuronData::statId=0;

int NeuronData::getId() const {
    return id;
}

void NeuronData::setId(int id) {
    NeuronData::id = id;
}

#endif //CNN_NEURONDATA_H
