//
// Created by user on 10/9/18.
//

#ifndef CNN_WEIGHTVECT_H//    Neuron ***nextLayer=createNeurons(nextLayerTotalRows,nextLayerTotalColumns,biasLink,activation);

#define CNN_WEIGHTVECT_H

#include "Weight.h"
#include "vector"

using namespace std;
class WeightVect{


private:
    vector<Weight*>* weightVect;
public:
    vector<Weight *> *getWeightVect() const {
        return weightVect;
    }

    void setWeightVect(vector<Weight *> *weightVect) {
        WeightVect::weightVect = weightVect;
    }

};
#endif //CNN_WEIGHTVECT_H
