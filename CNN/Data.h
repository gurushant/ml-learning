//
// Created by user on 19/10/18.
//
#include "vector"
#ifndef CNN_DATA_H
#define CNN_DATA_H
using namespace std;
class Data{
public:
    const vector<float> &getData() const {
        return data;
    }

    void setData(const vector<float> &data) {
        Data::data = data;
    }

private:vector<float> data;
};
#endif //CNN_DATA_H
