//
// Created by gurushant on 05/08/18.
//


#ifndef CNN_NEURON_H
#define CNN_NEURON_H


#include "Weight.h"
#include "NeuronData.h"
#include "string"

using namespace std;

class Link;
class BiasNeuron;
class Neuron;

using namespace std;
class Link{
public:
    Link(){
        statId++;
        setId(statId);
    }
    Neuron *getFromNeuron() const {
        return fromNeuron;
    }

    void setFromNeuron(Neuron *fromNeuron) {
        Link::fromNeuron = fromNeuron;
    }

    Neuron *getToNeuron() const {
        return toNeuron;
    }

    void setToNeuron(Neuron *toNeuron) {
        Link::toNeuron = toNeuron;
    }



    float getGradient() const {
        return gradient;
    }

    void setGradient(float gradient) {
        Link::gradient = gradient;
    }


public:
    Weight *getWeight() const {
        return weight;
    }

    void setWeight(Weight *weight) {
        Link::weight = weight;
    }


private:
    Neuron* fromNeuron;
    Neuron* toNeuron;
    Weight *weight;
    bool alive= true;
    bool volatileF=false;
public:
    bool isVolatileF() const {
        return volatileF;
    }

    void setVolatileF(bool volatileF) {
        Link::volatileF = volatileF;
    }

public:
    bool isAlive() const {
        return alive;
    }

    void setAlive(bool alive) {
        Link::alive = alive;
    }


private:
    float gradient=0;

private:
    static int statId;
    int id;
public:
    int getId() const {
        return id;
    }

    void setId(int id) {
        Link::id = id;
    }

};
int Link::statId=0;

class NeuronVect{
public:NeuronVect(){
    neuron=new vector<Neuron*>();
}
private:
    vector<Neuron*>* neuron;
public:
    vector<Neuron *> *getNeuron()  {
        return neuron;
    }

    void setNeuron(vector<Neuron *> *neuron) {
        NeuronVect::neuron = neuron;
    }

public:
    const string &getActivation() const {
        return activation;
    }

    void setActivation(const string &activation) {
        NeuronVect::activation = activation;
    }

private:
    string activation;
public:
    int getId() const {
        return id;
    }

    void setId(int id) {
        NeuronVect::id = id;
    }

private:
    int id=0;
};

class Neuron{
public:Neuron()
    {
        data=new vector<NeuronData*>();
        backLinks=new vector<Link*>();
        frontLinks=new vector<Link*>();;
        statId++;
        setId(statId);
    }
public:
    const string &getActivation() const {
        return activation;
    }

    void setActivation(const string &activation) {
        Neuron::activation = activation;
    }



private:
    string activation;
    string note;
    bool padded=false;
public:
    bool isPadded() const {
        return padded;
    }

    void setPadded(bool padded) {
        Neuron::padded = padded;
    }

public:
    const string &getNote() const {
        return note;
    }

    void setNote(const string &note) {
        Neuron::note = note;
    }

private:
public:
    vector<Link *> *getBackLinks() const {
        vector<Link *> *tempBackLinks=new vector<Link *>();
        for(int i=0;i<backLinks->size();i++)
        {
            Link* link=backLinks->at(i);
            if(link->isAlive())
            {
                tempBackLinks->push_back(link);
            }
        }
        return tempBackLinks;
    }

    vector<Link *> *getAllBackLinks() const {
        return backLinks;
    }

    void setBackLinks(vector<Link *> *backLinks) {
        Neuron::backLinks = backLinks;
    }

    vector<Link *> *getFrontLinks() const {

        vector<Link *> *tempFrontLinks=new vector<Link *>();
        for(int i=0;i<frontLinks->size();i++)
        {
            Link* link=frontLinks->at(i);
            if(link->isAlive())
            {
                tempFrontLinks->push_back(link);
            }
        }
        return tempFrontLinks;
    }

    vector<Link *> *getAllFrontLinks() const {
        return frontLinks;
    }

    void setFrontLinks(vector<Link *> *frontLinks) {
        Neuron::frontLinks = frontLinks;
    }

private:
    vector<Link*>* backLinks;
    vector<Link*>* frontLinks;
    vector<Neuron*>* poolNeuronVect=NULL;
public:
    vector<Neuron *> *getPoolNeuronVect() const {
        return poolNeuronVect;
    }

    void setPoolNeuronVect(vector<Neuron *> *poolNeuronVect) {
        Neuron::poolNeuronVect = poolNeuronVect;
    }

private:
    vector<NeuronData*>* data;
public:
    vector<NeuronData *> *getData() const {
        return data;
    }

    void setData(vector<NeuronData *> *data) {
        Neuron::data = data;
    }


public:
    int getId() const {
        return id;
    }

    void setId(int id) {
        Neuron::id = id;
    }

private:
     int id;
    static int statId;
public:
    BiasNeuron *getBiasNeuron() const {
        return biasNeuron;
    }

    void setBiasNeuron(BiasNeuron *biasNeuron) {
        Neuron::biasNeuron = biasNeuron;
    }

private:
    BiasNeuron* biasNeuron;

};
int Neuron::statId=0;

class BiasNeuron{
public:
    float getWeight() const {
        return weight;
    }

    void setWeight(float weight) {
        BiasNeuron::weight = weight;
    }

    const vector<Neuron *> &getToNeurons() const {
        return toNeurons;
    }

    void setToNeurons(const vector<Neuron *> &toNeurons) {
        BiasNeuron::toNeurons = toNeurons;
    }

    float getGradient() const {
        return gradient;
    }

    void setGradient(float gradient) {
        BiasNeuron::gradient = gradient;
    }

private:
    float weight;
    vector<Neuron*> toNeurons;
    float gradient;
    float gradientWithMomentum=0;
public:
    float getGradientWithMomentum() const {
        return gradientWithMomentum;
    }

    void setGradientWithMomentum(float gradientWithMomentum) {
        BiasNeuron::gradientWithMomentum = gradientWithMomentum;
    }
};


#endif //CNN_NEURON_H
