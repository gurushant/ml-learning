//
// Created by user on 4/11/18.
//
#include "vector"

#ifndef CNN_EXPECTEDOUTPUT_H
#define CNN_EXPECTEDOUTPUT_H
using namespace std;

class Output{
public:
    const vector<vector<float>> &getExpectedOutput() const {
        return expectedOutput;
    }

    void setExpectedOutput(const vector<vector<float>> &expectedOutput) {
        Output::expectedOutput = expectedOutput;
    }



private:
    vector<vector<float>> expectedOutput;

};
#endif //CNN_EXPECTEDOUTPUT_H
