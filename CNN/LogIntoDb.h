////
//// Created by gurushant on 05/08/18.
////

#ifndef CNN_LOGINTODB_H
#define CNN_LOGINTODB_H
#include <iostream>

#include <stdlib.h>
#include <iostream>
#include "mysql/include/jdbc/cppconn/connection.h"

#include "mysql/include/jdbc/cppconn/driver.h"
#include "mysql/include/jdbc/cppconn/exception.h"
#include "mysql/include/jdbc/cppconn/resultset.h"
#include "mysql/include/jdbc/cppconn/statement.h"
#include "mysql/include/jdbc/cppconn/prepared_statement.h"
#include "NeuronUtils.h"
#include "boost/lexical_cast.hpp"

using namespace std;
using namespace sql;

class MLDb{
private:        Driver *driver;
    Connection *con;
    Statement *stmt;
    ResultSet *res;
    PreparedStatement *getLayerIdSelectStmt;
    PreparedStatement *insertSuperLayerNeuralLayerMappingStmt;
    PreparedStatement *insertNeuronStmt;
    PreparedStatement *getNeuronStmt;

    PreparedStatement *insertWtStmt;
    PreparedStatement *getWtIdStmt;

    PreparedStatement *insertLinkStmt;
    PreparedStatement *getLinkStmt;

    PreparedStatement *insertNeuronData;

    PreparedStatement *insertBackLink;
    PreparedStatement *insertFrontLink;
    string insertNeuronQuery="insert into neuron(superLayerId,layerId,neuronId,channelIndex,iteration) values ";
    string insertLinkQuery="insert into link(weightId,linkId,fromNeuronId,toNeuronId,isForward,iteration,gradient) values ";
    string insertWeightQuery="insert into weight(weightId,weightValue,iteration,gradient) values ";
    string insertNDQuery="insert into neuronData(neuronId,neuronDataId,input,output,iteration,gradient)values";

public:MLDb() {
     /* Create a connection */
        driver = get_driver_instance();
        con = driver->connect("tcp://localhost:3306", "root", "root");
        con->setSchema("ml");
        stmt=con->createStatement();
//        getLayerIdSelectStmt = con->prepareStatement(
//                "select id from superLayerNeuronLayerMapping where superLayerId=? and neuronLayerId=?");
//
//        insertSuperLayerNeuralLayerMappingStmt=con->prepareStatement("insert into superLayerNeuronLayerMapping(superLayerId,neuronLayerId)"
//                                                                     "values(?,?)");

        insertNeuronStmt=con->prepareStatement("insert into neuron(superLayerId,layerId,neuronId,channelIndex)"
                                                "values(?,?,?,?),(?,?,?,?)");
//        getNeuronStmt=con->prepareStatement("select id from neuron where dbSuperLayerNeuronLayerMappingId=? and "
//                                            "neuronId=? and channelIndex=?");

        insertWtStmt=con->prepareStatement("insert into weight(weightId,weightValue)"
                                           "values(?,?)");

        getWtIdStmt=con->prepareStatement("select weightId from weight where weightId=?");

        insertLinkStmt=con->prepareStatement("insert into link(weightId,linkId,fromNeuronId,toNeuronId,isForward)"
                                             "values(?,?,?,?,?)");

//        getLinkStmt=con->prepareStatement("select id from link where dbWeightId=? and linkId=?");

        insertNeuronData=con->prepareStatement("insert into neuronData(neuronId,neuronDataId,input,output)"
                                               "values(?,?,?,?)");
//        insertBackLink=con->prepareStatement("insert into backLinks(dbNeuronId,dbLinkId)"
//                                              "values(?,?)");
//        insertFrontLink=con->prepareStatement("insert into frontLinks(dbNeuronId,dbLinkId)"
//                                             "values(?,?)");

    }

//    void insertSuperLayerMapping(int superLayerId,int layerId){
//        insertSuperLayerNeuralLayerMappingStmt->setInt(1,superLayerId);
//        insertSuperLayerNeuralLayerMappingStmt->setInt(2,layerId);
//        insertSuperLayerNeuralLayerMappingStmt->execute();
//    }
//
//    int getLayerId(int superLayerId,int layerId){
//        getLayerIdSelectStmt->setInt(1,superLayerId);
//        getLayerIdSelectStmt->setInt(2,layerId);
//        ResultSet *resultSet = getLayerIdSelectStmt->executeQuery();
//        if(resultSet->rowsCount()==0)
//        {
//            return -1;
//        } else{
//            resultSet->next();
//            return resultSet->getInt("id");
//        }
//
//    }
//    int fetchSuperLayerId(int superLayerId,int layerId){
//        int id=getLayerId(superLayerId,layerId);
//        if(id==-1){
//            insertSuperLayerMapping(superLayerId,layerId);
//            id=getLayerId(superLayerId,layerId);
//        }
//        return id;
//    }
//
//    int getNeuronId(int superLayerId,int layerId,int channel){
//        getNeuronStmt->setInt(1,superLayerId);
//        getNeuronStmt->setInt(2,layerId);
//        getNeuronStmt->setInt(3,channel);
//        ResultSet *resultSet = getNeuronStmt->executeQuery();
//        if(resultSet->rowsCount()==0)
//        {
//            return -1;
//        } else{
//            resultSet->next();
//            return resultSet->getInt("id");
//        }
//    }


    void deleteData(){
        stmt->execute("delete from link");
        stmt->execute("delete from neuron");
        stmt->execute("delete from neuronData");
        stmt->execute("delete from weight");

    }

    void executeQuery(){
        insertNeuronQuery.pop_back();
        insertLinkQuery.pop_back();
        insertWeightQuery.pop_back();
        insertNDQuery.pop_back();
        stmt->execute(insertNeuronQuery);
        stmt->execute(insertLinkQuery);
        stmt->execute(insertWeightQuery);
        stmt->execute(insertNDQuery);

        insertNeuronQuery="insert into neuron(superLayerId,layerId,neuronId,channelIndex,iteration) values ";
        insertLinkQuery="insert into link(weightId,linkId,fromNeuronId,toNeuronId,isForward,iteration,gradient) values ";
        insertWeightQuery="insert into weight(weightId,weightValue,iteration,gradient) values ";
        insertNDQuery="insert into neuronData(neuronId,neuronDataId,input,output,iteration,gradient)values";

    }

    void insertNeuron(int superLayerId,int layerId,int neuronId,int channel,
                      int iteration){
//        insertNeuronStmt->setInt(1,superLayerId);
//        insertNeuronStmt->setInt(2,layerId);
//        insertNeuronStmt->setInt(3,neuronId);
//        insertNeuronStmt->setInt(4,channel);
//        insertNeuronStmt->execute();
        insertNeuronQuery+="("+to_string(superLayerId)+","+to_string(layerId)+","+to_string(neuronId)+","
                +to_string(channel)+","+to_string(iteration)+"),";
    }

//    int fetchNeuronId(int superLayerId,int layerId,int channel){
//        int id=getNeuronId(superLayerId,layerId,channel);
//        if(id==-1){
//            insertNeuron(superLayerId,layerId,channel);
//            id=getNeuronId(superLayerId,layerId,channel);
//        }
//        return id;
//    }



//    int getWeightId(int weightId){
//        getWtIdStmt->setInt(1,weightId);
//        ResultSet *resultSet = getWtIdStmt->executeQuery();
//        if(resultSet->rowsCount()==0)
//        {
//            return -1;
//        } else{
//            resultSet->next();
//            return resultSet->getInt("weightId");
//        }
//    }

    void insertWeight(int weightId,float wtVal,int iteration,float gradient){
//        insertWtStmt->setInt(1,weightId);
//        insertWtStmt->setDouble(2,wtVal);
//        insertWtStmt->execute();
        insertWeightQuery+="("+to_string(weightId)+","+to_string(wtVal)+","+to_string(iteration)+","+
                to_string(gradient)+"),";
    }

//    int fetchWeightId(int weightId,float wtValue){
//        int id=getWeightId(weightId);
//        if(id==-1){
//            insertWeight(weightId,wtValue);
//            id=getWeightId(weightId);
//        }
//        return id;
//    }

    void insertLink(int weightId,int linkId,int neuronId,int toNeuronId,bool isForward,
                    int iteration,float gradient){
//        insertLinkStmt->setInt(1,weightId);
//        insertLinkStmt->setInt(2,linkId);
//        insertLinkStmt->setInt(3,neuronId);
//        insertLinkStmt->setInt(4,toNeuronId);
//        insertLinkStmt->setBoolean(5,isForward);
//        insertLinkStmt->execute();
          int i=0;
          if(isForward== true){
              i=1;
          }
          insertLinkQuery+="("+to_string(weightId)+","+to_string(linkId)+","+to_string(neuronId)
                  +","+to_string(toNeuronId)+","+to_string(i)+
                  +","+to_string(iteration)+","+to_string(gradient)+"),";
    }

//    int getLinkId(int weightId,int linkId){
//        getLinkStmt->setInt(1,weightId);
//        getLinkStmt->setInt(2,linkId);
//
//        ResultSet *resultSet = getLinkStmt->executeQuery();
//        if(resultSet->rowsCount()==0)
//        {
//            return -1;
//        } else{
//            resultSet->next();
//            return resultSet->getInt("id");
//        }
//    }


//    int fetchLinkId(int weightId,int linkId){
//        int id=getLinkId(weightId,linkId);
//        if(id==-1){
//            insertLink(weightId,linkId);
//            id=getLinkId(weightId,linkId);
//        }
//        return id;
//    }


    void insertND(int dbNeuronId,int neuronDataId,float input,float output,
                  int iteration,float gradient){
//        insertNeuronData->setInt(1,dbNeuronId);
//        insertNeuronData->setInt(2,neuronDataId);
//        insertNeuronData->setDouble(3,input);
//        insertNeuronData->setDouble(4,output);
//        insertNeuronData->execute();
        insertNDQuery+="("+to_string(dbNeuronId)+","+to_string(neuronDataId)+","+to_string(input)+","+
                to_string(output)+
                +","+to_string(iteration)+","+to_string(gradient)+"),";
    }

//    void insertBackNeuronLinks(int neuronid,int linkId){
//        insertBackLink->setInt(1,neuronid);
//        insertBackLink->setInt(2,linkId);
//        insertBackLink->execute();
//    }
//
//    void insertFrontNeuronLinks(int neuronid,int linkId){
//        insertFrontLink->setInt(1,neuronid);
//        insertFrontLink->setInt(2,linkId);
//        insertFrontLink->execute();
//    }

};

//void info(NeuronLayer* layer,int iteration)
//{
//    try
//    {
//        Driver *driver;
//        Connection *con;
//        Statement *stmt;
//        ResultSet *res;
//        PreparedStatement *pstmt;
//        PreparedStatement *selectStmt;
//        PreparedStatement *linkStmt;
//        PreparedStatement *linkInsert;
//        PreparedStatement *updateNeuronFrontLinkStmt;
//        PreparedStatement *updateNeuronBackLinkStmt;
//
//        /* Create a connection */
//        driver = get_driver_instance();
//        con = driver->connect("tcp://localhost:3306", "root", "root");
//        con->setSchema("ml");
//        stmt = con->createStatement();
//        pstmt = con->prepareStatement(
//                "INSERT INTO neurons (iteration_id, layer_id, neuron_id, input, output, activation, gradient) "
//                "VALUES (?,?,?,?,?,?,?)");
//        updateNeuronFrontLinkStmt = con->prepareStatement(
//                "update neurons set front_links=? where iteration_id=? and layer_id=? and "
//                "neuron_id=?");
//        updateNeuronBackLinkStmt = con->prepareStatement(
//                "update neurons set back_links=? where iteration_id=? and layer_id=? and "
//                "neuron_id=?");
//        selectStmt = con->prepareStatement(
//                "select id,front_links,back_links from neurons where iteration_id=? and layer_id=? and neuron_id=?");
//        linkStmt = con->prepareStatement("select id from links where iteration_id=? and layer_id=? and link_id=?");
//        linkInsert = con->prepareStatement(
//                "INSERT INTO links (iteration_id, layer_id,link_id, weight,gradient) VALUES (?,?,?,?,?)");
//
//
//        while (true) {
//            Neuron ***neurons = layer->getLayer();
//
//
//            for (int i = 0; i < layer->getRow(); i++) {
//                for (int j = 0; j < layer->getColumn(); j++) {
//
//                    Neuron *neuron = neurons[i][j];
//                    //insert the neuron
//                    pstmt->setInt64(1, iteration);
//                    pstmt->setInt64(2, layer->getId());
//                    pstmt->setInt64(3, neuron->getId());
////                pstmt->setDouble(4,neuron->getInput());
//                    vector<NeuronData*>*data=neuron->getData();
//                    string inputStr;
//                    for(NeuronData* data1:*data)
//                    {
//                        inputStr.append(",");
//                        inputStr.append(to_string(data1->getInput()));
//                    }
//
//                    pstmt->setString(4, inputStr);
////                pstmt->setDouble(5,neuron->getOutput());
//                    string outputStr;
//                    for(NeuronData* data1:*data)
//                    {
//                        outputStr.append(",");
//                        outputStr.append(to_string(data1->getOutput()));
//                    }
//                    pstmt->setString(5, outputStr);
//                    pstmt->setString(6, neuron->getActivation());
////                pstmt->setDouble(7,neuron->getGradient());
//                    string gradientStr;
//                    for(NeuronData* data1:*data)
//                    {
//                        gradientStr.append(",");
//                        gradientStr.append(to_string(data1->getGradient()));
//                    }
//                    pstmt->setString(7, gradientStr);
//                    pstmt->execute();
////            //fetch the neuron id(of neuron table) of newly added neuron entry.
////            selectStmt->setInt64(1,iteration);
////            selectStmt->setInt64(2,layer->getId());
////            selectStmt->setInt64(3,neuron->getId());
////            ResultSet* rSet=selectStmt->executeQuery();
//
//                    vector<Link *> frontLinks = neuron->getFrontLinks();
//                    vector<Link *> backLinks = neuron->getBackLinks();
////                    backLinks.clear();
////                Link* biasLink=neuron->getBiasLink();
//                    //Iterate over the front links.
//                    for (Link *link:frontLinks) {
//                        int weightId = link->getWeight()->getId();
//                        //check if link exists into the database
//                        linkStmt->setInt64(1, iteration);
//                        linkStmt->setInt64(2, layer->getId());
//                        linkStmt->setInt64(3, weightId);
//                        ResultSet *weightSelect = linkStmt->executeQuery();
//                        if (weightSelect->next()) {
//                            //update the front links
//                            selectStmt->setInt64(1, iteration);
//                            selectStmt->setInt64(2, layer->getId());
//                            selectStmt->setInt64(3, neuron->getId());
//                            ResultSet *rset = selectStmt->executeQuery();
//                            int linkTableId = weightSelect->getInt("id");
//                            //update the front link entry
//                            rset->next();
//                            string frontLinksStr = rset->getString("front_links");
//                            frontLinksStr.append(",");
//                            frontLinksStr.append(boost::lexical_cast<string>(linkTableId));
//                            //update the front links in a neurons table
//                            updateNeuronFrontLinkStmt->setString(1, frontLinksStr);
//                            updateNeuronFrontLinkStmt->setInt64(2, iteration);
//                            updateNeuronFrontLinkStmt->setInt64(3, layer->getId());
//                            updateNeuronFrontLinkStmt->setInt64(4, neuron->getId());
//                            updateNeuronFrontLinkStmt->execute();
//                        } else {
//                            //insert the entry into the link table
//                            linkInsert->setInt64(1, iteration);
//                            linkInsert->setInt64(2, layer->getId());
//                            linkInsert->setInt64(3, weightId);
//                            linkInsert->setString(4, to_string(link->getWeight()->getWeight()));
//                            float gradientWRTLink = vectorSum(link->getWeight()->getGradientVect());
//
//                            linkInsert->setDouble(5, gradientWRTLink);
//                            linkInsert->execute();
//
//                            //fetch the id
//                            linkStmt->setInt64(1, iteration);
//                            linkStmt->setInt64(2, layer->getId());
//                            linkStmt->setInt64(3, weightId);
//                            ResultSet *linkSelect = linkStmt->executeQuery();
//                            //update the front links
//                            if (linkSelect->next()) {
//                                weightId = linkSelect->getInt("id");
//                                selectStmt->setInt64(1, iteration);
//                                selectStmt->setInt64(2, layer->getId());
//                                selectStmt->setInt64(3, neuron->getId());
//                                ResultSet *rset = selectStmt->executeQuery();
//                                rset->next();
//                                string frontLinksStr = rset->getString("front_links");
//                                frontLinksStr.append(",");
//                                frontLinksStr.append(boost::lexical_cast<string>(weightId));
//                                //update the front links in a neurons table
//                                updateNeuronFrontLinkStmt->setString(1, frontLinksStr);
//                                updateNeuronFrontLinkStmt->setInt64(2, iteration);
//                                updateNeuronFrontLinkStmt->setInt64(3, layer->getId());
//                                updateNeuronFrontLinkStmt->setInt64(4, neuron->getId());
//                                updateNeuronFrontLinkStmt->execute();
//
//
//                            }
//                        }
//                    }
//                    //end of front link iteration
//                    //Iterate over the back links
//                    for (Link *link:backLinks) {
//                        int weightId = link->getWeight()->getId();
//                        //check if link exists into the database
//                        float backLayerId=layer->getId()-1;
//                        linkStmt->setInt64(1, iteration);
//                        linkStmt->setInt64(2, backLayerId);
//                        linkStmt->setInt64(3, weightId);
//                        ResultSet *weightSelect = linkStmt->executeQuery();
//                        if (weightSelect->next()) {
//                            //update the front links
//                            selectStmt->setInt64(1, iteration);
//                            selectStmt->setInt64(2, layer->getId());
//                            selectStmt->setInt64(3, neuron->getId());
//                            ResultSet *rset = selectStmt->executeQuery();
//                            int linkTableId = weightSelect->getInt("id");
//                            //update the front link entry
//                            rset->next();
//                            string backLinksStr = rset->getString("back_links");
//                            backLinksStr.append(",");
//                            backLinksStr.append(boost::lexical_cast<string>(linkTableId));
//                            //update the front links in a neurons table
//                            updateNeuronBackLinkStmt->setString(1, backLinksStr);
//                            updateNeuronBackLinkStmt->setInt64(2, iteration);
//                            updateNeuronBackLinkStmt->setInt64(3, layer->getId());
//                            updateNeuronBackLinkStmt->setInt64(4, neuron->getId());
//                            updateNeuronBackLinkStmt->execute();
//                        } else {
//                            //insert the entry into the link table
//                            linkInsert->setInt64(1, iteration);
//                            linkInsert->setInt64(2, layer->getId());
//                            linkInsert->setInt64(3, weightId);
//                            linkInsert->setDouble(4, link->getWeight()->getWeight());
//                            float gradientWRTLink = vectorSum(link->getWeight()->getGradientVect());
//                            linkInsert->setDouble(5, gradientWRTLink);
//                            linkInsert->execute();
//
//                            //fetch the id
//                            linkStmt->setInt64(1, iteration);
//                            linkStmt->setInt64(2, layer->getId());
//                            linkStmt->setInt64(3, weightId);
//                            ResultSet *linkSelect = linkStmt->executeQuery();
//                            //update the front links
//                            if (linkSelect->next()) {
//                                weightId = linkSelect->getInt("id");
//                                selectStmt->setInt64(1, iteration);
//                                selectStmt->setInt64(2, layer->getId());
//                                selectStmt->setInt64(3, neuron->getId());
//                                ResultSet *rset = selectStmt->executeQuery();
//                                rset->next();
//                                string backLinksStr = rset->getString("back_links");
//                                backLinksStr.append(",");
//                                backLinksStr.append(boost::lexical_cast<string>(weightId));
//                                //update the front links in a neurons table
//                                updateNeuronBackLinkStmt->setString(1, backLinksStr);
//                                updateNeuronBackLinkStmt->setInt64(2, iteration);
//                                updateNeuronBackLinkStmt->setInt64(3, layer->getId());
//                                updateNeuronBackLinkStmt->setInt64(4, neuron->getId());
//                                updateNeuronBackLinkStmt->execute();
//
//
//                            }
//                        }
//                    }
//                    //end of back link iterates
//                }
//            }
//
//            con->commit();
//            if(layer->getNextLayer()!=NULL) {
//                layer = *layer->getNextLayer();
//            } else{
//                break;
//            }
//
//        }
//
//        delete pstmt;
//        delete selectStmt;
//        delete con;
//    } catch (sql::SQLException &e) {
//        cout << "# ERR: SQLException in " << __FILE__;
//        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
//        cout << "# ERR: " << e.what();
//        cout << " (MySQL error code: " << e.getErrorCode();
//        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
//    }
//}
//
#endif //CNN_LOGINTODB_H
