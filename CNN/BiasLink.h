//
// Created by user on 11/8/18.
//

#ifndef CNN_LINK_H
#define CNN_LINK_H
#include "Neuron.h"


using namespace std;
class BiasLink{
public:
    Neuron *getFromNeuron() const {
        return fromNeuron;
    }

    void setFromNeuron(Neuron *fromNeuron) {
        BiasLink::fromNeuron = fromNeuron;
    }

    Neuron *getToNeuron() const {
        return toNeuron;
    }

    void setToNeuron(Neuron *toNeuron) {
        BiasLink::toNeuron = toNeuron;
    }



    float getGradient() const {
        return gradient;
    }

    void setGradient(float gradient) {
        BiasLink::gradient = gradient;
    }


public:
    Weight *getWeight() const {
        return weight;
    }

    void setWeight(Weight *weight) {
        BiasLink::weight = weight;
    }


private:
    Neuron* fromNeuron;
    Neuron* toNeuron;
    vector<Neuron*> toNeurons;
public:
    const vector<Neuron *> &getToNeurons() const {
        return toNeurons;
    }

    void setToNeurons(const vector<Neuron *> &toNeurons) {
        BiasLink::toNeurons = toNeurons;
    }

private:
    Weight *weight;

private:
    float gradient;



private:
    int id;
public:
    int getId() const {
        return id;
    }

    void setId(int id) {
        BiasLink::id = id;
    }

};

#endif //CNN_LINK_H
