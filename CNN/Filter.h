//
// Created by gurushant on 05/08/18.
//

#ifndef CNN_FILTER_H
#define CNN_FILTER_H
#include "Weight.h"
#include "Neuron.h"
#include "iostream"
#include "WeightVect.h"

using namespace std;
int weightIndex=0;
class Filter
{
public:
    int getRow() const {
        return row;
    }

    void setRow(int row) {
        Filter::row = row;
    }

    int getColumn() const {
        return column;
    }

    void setColumn(int column) {
        Filter::column = column;
    }

    void createWeight()
    {
        //create the weight matrix
        weightVect=new WeightVect**[row];
        for(int r=0;r<row;r++) {
            weightVect[r] = new WeightVect *[getColumn()];
        }
        //end of the weight matrix creation

        for(int i=0;i<getRow();i++)
        {
            for(int j=0;j<getColumn();j++)
            {
                //create an object of WeightVect
                weightVect[i][j]=new WeightVect();
                //End of WeightVect object creation
                vector<Weight*>* wtVect=new vector<Weight*>();
                for(int c=0;c<getChannel();c++)
                {
                    Weight* w=new Weight();
                    w->setWeight(getRandom());
                    wtVect->push_back(w);
                }
                weightVect[i][j]->setWeightVect(wtVect);
                weightIndex++;
            }
        }

        setWeightVect(weightVect);
    }


    float getRandom()
    {
        float LOW=-0.1;
        float HIGH=0.1;
        float randVal=(LOW+static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HIGH-LOW))));
        return randVal;
    }

private:
    int row;
    int column;
    WeightVect*** weightVect;
public:
    WeightVect ***getWeightVect() const {
        return weightVect;
    }

    void setWeightVect(WeightVect ***weightVect) {
        Filter::weightVect = weightVect;
    }

public:
    int getChannel() const {
        return channel;
    }

    void setChannel(int channel) {
        Filter::channel = channel;
    }

private:
    int channel;

public:
    BiasNeuron *getBiasNeuron() const {
        return biasNeuron;
    }

    void setBiasNeuron(BiasNeuron *biasNeuron) {
        Filter::biasNeuron = biasNeuron;
    }

private:
    BiasNeuron* biasNeuron;
};

#endif //CNN_FILTER_H