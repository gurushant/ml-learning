//
// Created by gurushant on 05/08/18.
//

#ifndef CNN_NEURONOPS_H
#define CNN_NEURONOPS_H

#include "NeuronLayer.h"
#include "vector"
#include "Filter.h"
#include "NeuronUtils.h"
#include "Activation.h"
#include "Filter.h"
#include "NeuronData.h"
#include "NeuralNetwork.h"
#include "algorithm"
#include "Output.h"
#include "SuperNeuronLayer.h"

void maxPool2D(NeuronLayer *layer);
void setBackLinkStatus(NeuronLayer *layer,bool, bool status);



void connectLayer(NeuronLayer* layer1,NeuronLayer* layer2)
{
    layer1->getNextNeuronLayerVect()->push_back(layer2);
    layer2->getPrevNeuronLayerVect()->push_back(layer1);

//    layer1->setNextLayer(layer2);
//    layer2->setPrevLayer(layer1);
//
//    if(layer1->getMaxPoolLayer()!=NULL)
//    {
//        NeuronLayer* maxPoolLayer=layer1->getMaxPoolLayer();
//        maxPoolLayer->setNextLayer(layer2);
//        layer2->setPrevLayer(maxPoolLayer);
//    }
}

void connectSuperLayer(SuperNeuronLayer* superNeuronLayer1,SuperNeuronLayer* superNeuronLayer2)
{
    superNeuronLayer1->setNextSuperLayer(superNeuronLayer2);
    superNeuronLayer2->setPrevSuperLayer(superNeuronLayer1);
}

NeuronLayer* convolve(NeuronLayer* layer,string activation,int stride,bool same,
        SuperNeuronLayer* superNeuronLayer,vector<Filter*>*filterVect) {
    //If there is max pool layer
    NeuronLayer* tempFromLayer=layer;
    if (layer->getMaxPoolLayer() != NULL) {
        layer = layer->getMaxPoolLayer();
    }
//    vector<Filter *> filterVect = *layer->getFilterVect();
    Filter *filter = filterVect->at(0);
    int filterH = filter->getColumn();
    int filterW = filter->getRow();

    int row = layer->getRow();
    int column = layer->getColumn();
    NeuronVect ***currentLayerNeuronVect = layer->getLayer();

    int nextLayerRows = 0;
    int nextLayerColumns = 0;
    if (same == true) {
//        int p=layer->getREndIndex()
        float tw = (stride * (layer->getRow() - 1) - layer->getRow() + filterW)/2.0;
        float th = (stride * (layer->getRow() - 1) - layer->getRow() + filterW)/2.0;

        int pw=0;
        int ph=0;

        if((int(tw)==tw) && (int(th)==th))
        {
            pw=tw;
            ph=th;
        }
        else
        {
            cout<<"XXXXXXXXXXXXXX "<<layer->getNote() <<" does not have correct numbered filters"<<" XXXXXXXXXXXXXX"<<endl;
            exit(0);
        }

        layer->setRow(layer->getRow() + 2*ph);
        layer->setColumn(layer->getColumn() + 2*pw);
        layer->setRowStartIndex(layer->getRowStartIndex() - pw);
        layer->setRowEndIndex(layer->getRowEndIndex() + pw);

        layer->setColStartIndex(layer->getColStartIndex() - pw);
        layer->setColEndIndex(layer->getColEndIndex() + pw);

        row = layer->getRow();
        column = layer->getColumn();
    }
    nextLayerRows = ((row - filterW) / stride) + 1;
    nextLayerColumns = ((column - filterH) / stride) + 1;

    NeuronVect ***nextLayer = createNeurons(nextLayerRows, nextLayerColumns, filterVect->size(), activation,
                                            "");

    vector<BiasNeuron *> *biasNeuronVect = new vector<BiasNeuron *>();
    int ch = filterVect->size();

    NeuronLayer *nextLayerNeurons = createNeuronLayer(nextLayer, nextLayerRows, nextLayerColumns, ch,superNeuronLayer);


    int rowIndex = 0;
    const int rowStartIndex1 = layer->getRowStartIndex();
    const int rowEndIndex1 = layer->getRowEndIndex();
    const int colStartIndex1 = layer->getColStartIndex();
    const int colEndIndex1 = layer->getColEndIndex();



    for (int f = 0; f < filterVect->size(); f++) {
        //Bias Level Settings
        filter = filterVect->at(f);
        BiasNeuron *biasNeuron = new BiasNeuron();
        biasNeuron->setWeight(0);
        filter->setBiasNeuron(biasNeuron);
        //End of Bias Level Settings
        vector<Neuron *> linkVect = biasNeuron->getToNeurons();
        WeightVect ***wVect = filter->getWeightVect();
        int nextLayerRowStartIndex = nextLayerNeurons->getRowStartIndex();

        for (int r = rowStartIndex1; (r+filterH-1) < rowEndIndex1; r+=stride) {
            int nextLayerColStartIndex = nextLayerNeurons->getColStartIndex();

            for (int c = colStartIndex1; (c+filterW-1) < colEndIndex1;c+=stride) {
                vector<Neuron *> *nvect = nextLayer[nextLayerRowStartIndex][nextLayerColStartIndex]->getNeuron();
                nextLayerColStartIndex++;

                Neuron *toNeuronObj = nvect->at(f);
                toNeuronObj->setBiasNeuron(biasNeuron);
                vector<Link *> *backLinks = toNeuronObj->getAllBackLinks();

                ////Prepare Receptive Field
                for (int i = r, fw = 0; i < (r + filterW); i++, fw++) {
                    for (int j = c, fh = 0; j < (c + filterH); j++, fh++) {
                        vector<Neuron *>* receptiveNeuronVect = currentLayerNeuronVect[i][j]->getNeuron();
                        vector<Weight *> *wtVect = wVect[fw][fh]->getWeightVect();
                        //iterate through the channel
                        for (int n = 0; n < receptiveNeuronVect->size(); n++) {
                            Neuron *fromNeuronObj = receptiveNeuronVect->at(n);
                            Link *link = createLink();
                            //Assign the weight value to the link
                            link->setWeight(wtVect->at(n));
                            link->setFromNeuron(fromNeuronObj);
                            link->setToNeuron(toNeuronObj);
                            vector<Link *> *frontLinks = fromNeuronObj->getAllFrontLinks();
                            frontLinks->push_back(link);
//                            fromNeuronObj->setFrontLinks(frontLinks);
                            //Assign back links
                            backLinks->push_back(link);
                        }

                    }
                }
                linkVect.push_back(toNeuronObj);
            }
            nextLayerRowStartIndex++;
        }
        biasNeuron->setToNeurons(linkVect);
        biasNeuronVect->push_back(biasNeuron);
    }
    nextLayerNeurons->setBiasNeuronVect(biasNeuronVect);
    nextLayerNeurons->setFilterVect(filterVect);
    connectLayer(tempFromLayer,nextLayerNeurons);
    return nextLayerNeurons;
}


NeuronLayer* convolveDense(NeuronLayer* layer,string activation, int stride,bool same,
        SuperNeuronLayer* superLayer,vector<Filter*>*filterVect) {
    //If there is max pool layer
    NeuronLayer* tempFromLayer=layer;
    if (layer->getMaxPoolLayer() != NULL) {
        layer = layer->getMaxPoolLayer();
    }
    Filter *filter = filterVect->at(0);
    int filterH = filter->getColumn();
    int filterW = filter->getRow();

    int row = layer->getRow();
    int column = layer->getColumn();
    NeuronVect ***currentLayerNeuronVect = layer->getLayer();

    int nextLayerRows = 0;
    int nextLayerColumns = 0;
    nextLayerRows = ((row - filterW) / stride) + 1;
    nextLayerColumns = ((column - filterH) / stride) + 1;

    NeuronVect ***nextLayer = createNeurons(nextLayerRows, nextLayerColumns,
            filterVect->size(), activation,
                                            "convolve1");

    vector<BiasNeuron *> *biasNeuronVect = new vector<BiasNeuron *>();
    int ch = filterVect->size();

    NeuronLayer *nextLayerNeurons = createNeuronLayer(nextLayer, nextLayerRows, nextLayerColumns, ch,superLayer);


    int rowIndex = 0;
    const int rowStartIndex1 = layer->getRowStartIndex();
    const int rowEndIndex1 = layer->getRowEndIndex();
    const int colStartIndex1 = layer->getColStartIndex();
    const int colEndIndex1 = layer->getColEndIndex();
    BiasNeuron *biasNeuron = new BiasNeuron();
    biasNeuron->setWeight(0);
    vector<Neuron *> linkVect = biasNeuron->getToNeurons();
    filterVect->at(0)->setBiasNeuron(biasNeuron);

    for (int f = 0; f < filterVect->size(); f++) {
        filter = filterVect->at(f);
        //End of Bias Level Settings
        WeightVect ***wVect = filter->getWeightVect();
        int nextLayerRowStartIndex = nextLayerNeurons->getRowStartIndex();

        for (int r = rowStartIndex1; (r+filterH-1) < rowEndIndex1; r+=stride) {
            int nextLayerColStartIndex = nextLayerNeurons->getColStartIndex();

            for (int c = colStartIndex1; (c+filterW-1) < colEndIndex1;c+=stride) {
                vector<Neuron *> *nvect = nextLayer[nextLayerRowStartIndex][nextLayerColStartIndex]->getNeuron();

                Neuron *toNeuronObj = nvect->at(f);
                toNeuronObj->setBiasNeuron(biasNeuron);
                vector<Link *> *backLinks = toNeuronObj->getAllBackLinks();

                ////Prepare Receptive Field
                for (int i = r, fw = 0; i < (r + filterW); i++, fw++) {
                    for (int j = c, fh = 0; j < (c + filterH); j++, fh++) {
                        vector<Neuron *>* receptiveNeuronVect = currentLayerNeuronVect[i][j]->getNeuron();
                        vector<Weight *> *wtVect = wVect[fw][fh]->getWeightVect();
                        //iterate through the channel
                        for (int n = 0; n < receptiveNeuronVect->size(); n++) {
                            Neuron *fromNeuronObj = receptiveNeuronVect->at(n);
                            Link *link = createLink();
                            //Assign the weight value to the link
                            link->setWeight(wtVect->at(n));
                            link->setFromNeuron(fromNeuronObj);
                            link->setToNeuron(toNeuronObj);
                            vector<Link *> *frontLinks = fromNeuronObj->getAllFrontLinks();
                            frontLinks->push_back(link);
                            //Assign back links
                            backLinks->push_back(link);
                        }

                    }
                }
                linkVect.push_back(toNeuronObj);
                nextLayerColStartIndex++;

            }
            nextLayerRowStartIndex++;
        }

    }
    biasNeuron->setToNeurons(linkVect);
    biasNeuronVect->push_back(biasNeuron);

    nextLayerNeurons->setBiasNeuronVect(biasNeuronVect);
    connectLayer(tempFromLayer,nextLayerNeurons);
    nextLayerNeurons->setFilterVect(filterVect);
    return nextLayerNeurons;
}

NeuronLayer* flatten(NeuronLayer* neuronLayer,NeuronLayer* prevNeuronLayer)
{
    NeuronLayer* tempFromLayer=neuronLayer;
    int totalRows=neuronLayer->getRow()*neuronLayer->getColumn()*neuronLayer->getChannel();
    NeuronVect*** toLayer=createNeuronEmpty(totalRows,1,"");
    NeuronVect*** fromLayer=neuronLayer->getLayer();
    int rowIndex=0;
    for(int r=neuronLayer->getRowStartIndex();r<neuronLayer->getRowEndIndex();r++)
    {
        for(int c=neuronLayer->getColStartIndex();c<neuronLayer->getColEndIndex();c++)
        {
            vector<Neuron*>* neurons= fromLayer[r][c]->getNeuron();
            for(int ch=0;ch<neurons->size();ch++)
            {
                vector<Neuron*>* to=new vector<Neuron*>();
                to->push_back(neurons->at(ch));
                toLayer[rowIndex][0]->setNeuron(to);
                rowIndex++;
            }
        }
    }


    NeuronLayer *flattenNeuronLayer=new NeuronLayer();
    flattenNeuronLayer->setRow(totalRows);
    flattenNeuronLayer->setColumn(1);
    flattenNeuronLayer->setLayer(toLayer);
    flattenNeuronLayer->setId(neuronLayer->getId());

    flattenNeuronLayer->setRowStartIndex(0);
    flattenNeuronLayer->setRowEndIndex(totalRows);
    flattenNeuronLayer->setColStartIndex(0);
    flattenNeuronLayer->setColEndIndex(1);
    flattenNeuronLayer->setBiasNeuronVect(neuronLayer->getBiasNeuronVect());
//    vector<NeuronLayer*>*nextLayerNeuronVect= prevNeuronLayer->getNextLayerVect();
//    int vectSize=nextLayerNeuronVect->size();
//    nextLayerNeuronVect->erase(nextLayerNeuronVect->begin()+vectSize-1);
    connectLayer(prevNeuronLayer,flattenNeuronLayer);
    return flattenNeuronLayer;

}

NeuronLayer* dense(NeuronLayer *layer,string activation,SuperNeuronLayer* superNeuronLayer,
                   vector<Filter*>*filterVect)
{
//    vector<Filter*>*filterVect=layer->getFilterVect();
    Filter* filter=filterVect->at(0);

    NeuronVect ***toNeuronVects=createDenseNeurons(filter->getColumn(),activation,"dense");


    NeuronVect*** fromNeuronVects=layer->getLayer();
    WeightVect*** weightVect=filter->getWeightVect();

    //Bias settings for the layer
    BiasNeuron* biasNeuron=new BiasNeuron();
    biasNeuron->setWeight(0);
    vector<Neuron*> biasToNeuronVect=biasNeuron->getToNeurons();
    filter->setBiasNeuron(biasNeuron);
    //end of bias settings for layer
    vector<BiasNeuron*> *biasNeuronVect=new vector<BiasNeuron*>();
    biasNeuronVect->push_back(biasNeuron);
    //Dot product for DENSE layer
    for (int i = 0; i < filter->getColumn(); i++) {
        //Following is for back prop
        vector<Neuron*>*toNeuronVect=toNeuronVects[i][0]->getNeuron();
        Neuron *toNeuron = toNeuronVect->at(0);
        biasToNeuronVect.push_back(toNeuron);
        //Push bias neuron in the vector if BiasNeuron class
//        if(toNeuron->getActivation().compare("linear")!=0) {
//            toNeuron->setBiasNeuron(biasNeuron);
//        }
        toNeuron->setBiasNeuron(biasNeuron);

        vector<Link *>* backLinks = toNeuron->getAllBackLinks();
        float neuronOutput=0;
        for (int j = 0; j < filter->getRow(); j++) {
            vector<Neuron*>*fromNeuronVect=fromNeuronVects[j][0]->getNeuron();
            Neuron *fromNeuron = fromNeuronVect->at(0);

            Link *link = createLink();
            link->setFromNeuron(fromNeuron);
            link->setToNeuron(toNeuron);
            vector<Weight*>* wtVect=weightVect[j][i]->getWeightVect();
            Weight* w=wtVect->at(DENSE_INDEX);
            link->setWeight(w);

            vector<Link *>* frontLinks = fromNeuron->getAllFrontLinks();
            frontLinks->push_back(link);
            backLinks->push_back(link);
        }
    }

    biasNeuron->setToNeurons(biasToNeuronVect);
    NeuronLayer *neuronLayer=createNeuronLayerForDense(toNeuronVects,filter->getColumn());
    superNeuronLayer->addNeuronLayer(neuronLayer);
    neuronLayer->setBiasNeuronVect(biasNeuronVect);
    neuronLayer->setFilterVect(filterVect);
    connectLayer(layer,neuronLayer);

    return neuronLayer;

}


void setActivation(NeuronLayer* layer ){
    NeuronVect*** neurons=layer->getLayer();
    vector<BiasNeuron*> *biasNeuronVect=layer->getBiasNeuronVect();

    for(int i=4;i<layer->getRow();i++)
    {
        for(int j=0;j<layer->getColumn();j++)
        {
            vector<Neuron*>* neuronVect=neurons[i][j]->getNeuron();
            for(int ch=0;ch<layer->getChannel();ch++)
            {
                Neuron* neuron=neuronVect->at(ch);
                neuron->setActivation("tanh");
            }
        }
    }
}



float getActivationValue(string activation,float value)
{
    if(activation=="sigmoid")
    {
        return sigmoid(value);
    }
    else
    if(activation=="tanh")
    {
        return sigmoidTanh(value);
    }
    else
    if(activation=="softmax")
    {
        return softmax(value);
    }
    else
    if(activation=="relu")
    {
        return relu(value);
    }else
    if(activation==BOUNDING_BOX_ACTIVATION)
    {
        return value;
    }

}

float getActivationDerivative(string activation,float value)
{
    if(activation=="sigmoid")
    {
        return dsigmoid(value);
    }
    else
    if(activation=="tanh")
    {
        return dSigmoidTanh(value);
    }
    else
    if(activation=="softmax")
    {
        return value*(1-value);
    }
    else
    if(activation=="relu")
    {
        return drelu(value);
    }

}


void forwardPropDev(SuperNeuronLayer* superLayer) {
    SuperNeuronLayer* tmpSuperLayer=superLayer;

    int totalIterations = 0;
    superLayer=superLayer->getNextSuperLayer();
    while (superLayer != NULL) {
        vector<NeuronLayer *> *layers = superLayer->getNeuronLayer();

        for (NeuronLayer *layer:*layers) {
//            cout<<layer->getNote()<<endl;
            //Store varible for softmax processing
            vector<float> softmaxOutputData;
            for (int b = 0; b < BATCH_SIZE; b++) {
                softmaxOutputData.push_back(0);
            }
            //End of Store variable softmax processing
            NeuronVect ***toNeuronVects = layer->getLayer();
            const int rowEndIndex = layer->getRowEndIndex();
            const int colEndIndex = layer->getColEndIndex();


            for (int r = layer->getRowStartIndex(); r < rowEndIndex; r++) {
                for (int c = layer->getColStartIndex(); c < colEndIndex; c++) {
                    vector<Neuron *> *toNeuronVect = toNeuronVects[r][c]->getNeuron();
                    for (int ch = 0; ch < layer->getChannel(); ch++) {
                        totalIterations += 1;
                        Neuron *toNeuron = toNeuronVect->at(ch);
                        vector<Link *> *backLinks = toNeuron->getBackLinks();
                        vector<NeuronData *> *toNeuronDataVect = toNeuron->getData();

                        for (int b = 0; b < BATCH_SIZE; b++) {
                            float input = 0;
                            NeuronData *nd = toNeuronDataVect->at(b);
                            for (Link *link:*backLinks) {
                                vector<NeuronData *> *fromNeuronDataVects = link->getFromNeuron()->getData();
                                float neuronInput = fromNeuronDataVects->at(b)->getOutput();
                                input += neuronInput * link->getWeight()->getWeight();

                            }
                            if (toNeuron->getBiasNeuron() != NULL) {
//                            cout<<layer->getNote()<<","<<toNeuron->getBiasNeuron()->getWeight()<<endl;
                                input += toNeuron->getBiasNeuron()->getWeight();
                            }

                            float output = getActivationValue(toNeuron->getActivation(), input);
                            if (toNeuron->getActivation().compare("softmax") == 0) {
                                softmaxOutputData[b] += output;
                            }
                            nd->setOutput(output);
                            nd->setInput(input);
                        }
                    }
                }
            }
            MaxPool *maxPool = layer->getMaxPool();
            if (maxPool != NULL) {
                maxPool2D(layer);
            }
            ///Put logic for softmax
            if (layer->getLayer()[0][0]->getActivation().compare("softmax") == 0) {
                NeuronVect ***toNeuronVects = layer->getLayer();
                const int rowEndIndex = layer->getRowEndIndex();
                const int colEndIndex = layer->getColEndIndex();
                for (int r = layer->getRowStartIndex(); r < rowEndIndex; r++) {
                    for (int c = layer->getColStartIndex(); c < colEndIndex; c++) {
                        vector<Neuron *> *toNeuronVect = toNeuronVects[r][c]->getNeuron();
                        for (int ch = 0; ch < layer->getChannel(); ch++) {
                            Neuron *toNeuron = toNeuronVect->at(ch);
                            if (toNeuron->getActivation().compare("softmax") == 0) {
                                vector<NeuronData *> *toNeuronDataVect = toNeuron->getData();
                                for (int b = 0; b < BATCH_SIZE; b++) {
                                    NeuronData *nd = toNeuronDataVect->at(b);
                                    if (softmaxOutputData[b] == 0) {
                                        softmaxOutputData[b] = 1e-10;
                                    }
                                    nd->setOutput(nd->getOutput() / softmaxOutputData[b]);
                                }
                            }
                        }
                    }
                }
            }
            ///End of Put logic for softmax
        }
        superLayer = superLayer->getNextSuperLayer();
    }
    //Calculate output of softmax
//    superLayer=tmpSuperLayer;
//    while(superLayer!= NULL) {
//        vector<NeuronLayer *> *layers = superLayer->getNeuronLayer();
//        for (NeuronLayer *layer:*layers) {
//            if (layer->getLayer()[0][0]->getActivation().compare("softmax") == 0) {
//                NeuronVect ***toNeuronVects = layer->getLayer();
//                const int rowEndIndex = layer->getRowEndIndex();
//                const int colEndIndex = layer->getColEndIndex();
//                for (int r = layer->getRowStartIndex(); r < rowEndIndex; r++) {
//                    for (int c = layer->getColStartIndex(); c < colEndIndex; c++) {
//                        vector<Neuron *> *toNeuronVect = toNeuronVects[r][c]->getNeuron();
//                        for (int ch = 0; ch < layer->getChannel(); ch++) {
//                            Neuron *toNeuron = toNeuronVect->at(ch);
//                            if (toNeuron->getActivation().compare("softmax") == 0) {
//                                vector<NeuronData *> *toNeuronDataVect = toNeuron->getData();
//                                for (int b = 0; b < BATCH_SIZE; b++) {
//                                    NeuronData *nd = toNeuronDataVect->at(b);
//                                    if (softmaxOutputData[b] == 0) {
//                                        softmaxOutputData[b] = 1e-10;
//                                    }
//                                    nd->setOutput(nd->getOutput() / softmaxOutputData[b]);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        superLayer=superLayer->getNextSuperLayer();
//    }
}




vector<vector<float>> calculateErrorSoftmaxDense(NeuronLayer* outputLayer,
        vector<vector<float>> expectedOutputs)
{
    vector<vector<float>> outputErrors;
    NeuronVect ***outputVect=outputLayer->getLayer();

    for(int b=0;b<BATCH_SIZE;b++) {
        vector<float> outputError;
        vector<float> expectedOutput=expectedOutputs.at(b);

        ///Calculate the error for output class type ///
        for (int i = outputLayer->getRowStartIndex(); i < outputLayer->getRowEndIndex(); i++) {
            vector<Neuron*>* ns=outputVect[i][DENSE_INDEX]->getNeuron();
//            cout<<ns->at(DENSE_INDEX)->getActivation()<<endl;
            vector<NeuronData *>* neuronData = ns->at(DENSE_INDEX)->getData();
            float output = neuronData->at(b)->getOutput();
            float expectedOut = expectedOutput[i];

            if (output == 0) {
                output=0.000000001;
            }
            if(ns->at(b)->getActivation().compare("softmax")==0) {
                outputError.push_back(-(expectedOut * log(output)));
            } else
            if(ns->at(b)->getActivation().compare(BOUNDING_BOX_ACTIVATION)==0) {
                outputError.push_back((output-expectedOut)*(output-expectedOut));
            }
        }
        ///End of Calculate the error for output class type ///

        outputErrors.push_back(outputError);
    }
    return outputErrors;
}

vector<vector<float>> calculateErrorSoftmaxConv(NeuronLayer* outputLayer,vector<vector<float>> expectedOutputs)
{
    vector<vector<float>> outputErrors;
    NeuronVect ***outputVect=outputLayer->getLayer();

    for(int b=0;b<BATCH_SIZE;b++) {
        vector<float> outputError;
        vector<float> expectedOutput=expectedOutputs[b];

        for (int i = outputLayer->getRowStartIndex(); i < outputLayer->getRowEndIndex(); i++) {
            for (int j = outputLayer->getColStartIndex(); j < outputLayer->getColEndIndex();j++)
            {
                vector<Neuron *> *ns = outputVect[i][j]->getNeuron();
                for(int c=0;c<outputLayer->getChannel();c++) {
                    vector<NeuronData *> *neuronData = ns->at(c)->getData();
                    float output = neuronData->at(b)->getOutput();
                    float expectedOut = expectedOutput[c];

                    if (output == 0) {
                        output = 1e-5;
                    }
//                    cout<<"Expected "<<expectedOut<<endl;
//                    cout<<"output1 "<<output<<endl;

                    outputError.push_back(-(expectedOut * log(output)));
                }
            }
        }
        outputErrors.push_back(outputError);
    }
    return outputErrors;
}


vector<vector<float>> calculateLinearError(NeuronLayer* outputLayer,vector<vector<float>> expectedOutputs)
{
    vector<vector<float>> outputErrors;
    NeuronVect ***outputVect=outputLayer->getLayer();

    for (int r = outputLayer->getRowStartIndex(); r < outputLayer->getRowEndIndex(); r++) {

        vector<Neuron *> ns = *outputVect[r][DENSE_INDEX]->getNeuron();
        vector<NeuronData *> neuronData = *ns[DENSE_INDEX]->getData();
        vector<float> expectedOutput = expectedOutputs[r];
        vector<float> outputError;
        for (int b = 0; b < BATCH_SIZE; b++) {
            float output = neuronData[DENSE_INDEX][b].getOutput();
            float expectedOut = expectedOutputs[b][DENSE_INDEX];
            outputError.push_back((output-expectedOut)*(output-expectedOut));
        }
        outputErrors.push_back(outputError);

    }
    return outputErrors;
}

vector<vector<float>> calculateError(NeuronLayer* outputLayer,vector<vector<float>> expectedOutputs,
                                     int flag)
{
    if(outputLayer->getLayer()[0][0]->getActivation()=="softmax")
    {
        if(flag==1)
        return  calculateErrorSoftmaxConv(outputLayer,expectedOutputs);
        else
            return  calculateErrorSoftmaxDense(outputLayer,expectedOutputs);

    } else
    if(outputLayer->getLayer()[0][0]->getActivation()=="linear")
    {
            return  calculateLinearError(outputLayer,expectedOutputs);
    }
    else {
        vector<vector<float>> outputErrors;
        if(outputLayer->getLayer()[0][0]->getActivation()=="sigmoid") {
            NeuronVect ***outputVect = outputLayer->getLayer();

            for (int r = outputLayer->getRowStartIndex(); r < outputLayer->getRowEndIndex(); r++) {

                vector<Neuron *> ns = *outputVect[r][DENSE_INDEX]->getNeuron();
                vector<NeuronData *> neuronData = *ns[DENSE_INDEX]->getData();
                vector<float> expectedOutput = expectedOutputs[r];
                vector<float> outputError;
                for (int b = 0; b < BATCH_SIZE; b++) {
                    float output = neuronData[DENSE_INDEX][b].getOutput();
                    float expectedOut = expectedOutputs[b][DENSE_INDEX];
                    //apply error function
                    float error = 0;

                    if (expectedOut == 0) {
                        error = log(1 - output);
                    } else if (expectedOut == 1) {
                        if (output == 0) {
                            output = 0.000001;
                        }
                        error = log(output);
                    }
                    outputError.push_back(-error);
                }
                outputErrors.push_back(outputError);

            }
        }
        return outputErrors;
    }
}

vector<vector<float>>  derivativeErrorSoftmax(vector<vector<float>> expectedOutputs,vector<vector<float>> actualOutputs)
{
    vector<vector<float>> outputDError;
    vector<vector<float>> derivativeErrors;
    for(int b=0;b<BATCH_SIZE;b++)
    {
        vector<float> expectedOutput=expectedOutputs[b];
        vector<float> actualOutput=actualOutputs[b];
        vector<float> derivative;
        for(int c=0;c<actualOutput.size();c++)
        {
            derivative.push_back(actualOutput[c]-expectedOutput[c]);
        }
        derivativeErrors.push_back(derivative);
    }
    return derivativeErrors;
}

vector<vector<float>>  derivativeErrorLinear(vector<vector<float>> expectedOutputs,vector<vector<float>> actualOutputs)
{
    vector<vector<float>> outputDError;
    vector<vector<float>> derivativeErrors;
    for(int i=0;i<expectedOutputs.size();i++)
    {
        vector<float> expectedOutput=expectedOutputs[i];
        vector<float> actualOutput=actualOutputs[i];
        vector<float> derivative;
        for(int c=0;c<actualOutput.size();c++){
            derivative.push_back(actualOutput[c]-expectedOutput[c]);
        }
        derivativeErrors.push_back(derivative);

    }
//    for(int b=0;b<BATCH_SIZE;b++)
//    {
//        vector<float> expectedOutput=expectedOutputs[b];
//        vector<float> actualOutput=actualOutputs[b];
//        vector<float> derivative;
//        for(int c=0;c<actualOutput.size();c++)
//        {
//            derivative.push_back(actualOutput[c]-expectedOutput[c]);
//        }
//        derivativeErrors.push_back(derivative);
//    }

    return derivativeErrors;
}


vector<vector<float>> derivativeError(vector<vector<float>> expectedOutputs,vector<vector<float>> actualOutputs
        ,string activation)
{
    if(activation=="softmax")
    {
        return derivativeErrorSoftmax(expectedOutputs,actualOutputs);
    } else
    if(activation=="linear")
    {
        return derivativeErrorLinear(expectedOutputs,actualOutputs);
    }
    else {
        vector<vector<float>> outputDError;
        vector<vector<float>> derivativeErrors;
        for(int b=0;b<BATCH_SIZE;b++)
        {
            vector<float> expectedOutput=expectedOutputs[b];
            vector<float> actualOutput=actualOutputs[b];
            vector<float> derivative;
            for(int c=0;c<actualOutput.size();c++)
            {
                derivative.push_back(actualOutput[c]-expectedOutput[c]);
            }
            derivativeErrors.push_back(derivative);
        }

        return derivativeErrors;
    }
}

float sumVector(vector<vector<float>> vect)
{
    float total=0;
    for(vector<float>vector1:vect) {
        for (float f:vector1) {
            total += f;
        }
    }
    return total;
}

float sumVector(vector<float> vect)
{
    float total=0;
    for(float val:vect) {
        total += val;
    }
    return total;
}

void printVector(vector<float> vector1)
{
    for(float v:vector1)
    {
        cout<<v<<endl;
    }
}

vector<vector<float>> getVectorData(NeuronLayer* layer,string dataType)
{
    NeuronVect*** neuronVec=layer->getLayer();

    vector<vector<float>> dataVect;
    for(int i=0;i<layer->getRow();i++)
    {
        for(int j=0;j<layer->getColumn();j++)
        {
            if(dataType.compare("output")==0)
            {
                vector<Neuron*> neuronV= *neuronVec[i][j]->getNeuron();
                vector<NeuronData*> neuronData= *neuronV[DENSE_INDEX]->getData();
                vector<float> data;
                for(int b=0;b<BATCH_SIZE;b++) {
                    NeuronData* nd=neuronData[b];
                    data.push_back(nd->getOutput());
                }
                dataVect.push_back(data);
            }
            else
            if(dataType.compare("gradient")==0)
            {
                vector<Neuron*> neuronV= *neuronVec[i][j]->getNeuron();
                vector<NeuronData*> neuronData= *neuronV[DENSE_INDEX]->getData();

                vector<float> data;
                for(int b=0;b<BATCH_SIZE;b++) {
                    NeuronData* nd=neuronData[b];
                    data.push_back(nd->getGradient());
                }
                dataVect.push_back(data);
            }
        }
    }
    return dataVect;
}

float backPropDev(SuperNeuronLayer* superOutputLayer)
{

    int flag=1;
    float totalError=0;
    while (superOutputLayer!=NULL) {
        vector<NeuronLayer *> *neuronLayers = superOutputLayer->getNeuronLayer();
        reverse(neuronLayers->begin(),neuronLayers->end());
        for (NeuronLayer *neuronLayer:*neuronLayers) {

            if(neuronLayer->isOutputLayer()) {
                Output* outputE=neuronLayer->getExpectedOutput();

                vector<vector<float>> expectedOutputs = outputE->getExpectedOutput();
                vector<vector<float>> arrErrors = calculateError(neuronLayer, expectedOutputs,
                                                                 flag);
                totalError = sumVector(arrErrors);

                cout << "Error is "+neuronLayer->getNote()<<"--" << totalError << endl;

                vector<vector<float>> actualOutput = getVectorData(neuronLayer, "output");
                int totalIterations = 0;

                NeuronVect ***neuronVects = neuronLayer->getLayer();
                if (neuronLayer->getLayer()[0][0]->getActivation() == "sigmoid") {
                    vector<vector<float>> derrors = derivativeError(expectedOutputs, actualOutput,
                                                                    neuronLayer->getLayer()[0][0]->getActivation());

                    const int rowStartIndex = neuronLayer->getRowStartIndex();
                    const int rowEndIndex = neuronLayer->getRowEndIndex();
                    const int colStartIndex = neuronLayer->getColStartIndex();
                    const int colEndIndex = neuronLayer->getColEndIndex();

                    for (int i = rowStartIndex; i < rowEndIndex; i++) {
                        for (int j = colStartIndex; j < colEndIndex; j++) {

                            vector<Neuron *> neuronVect = *neuronVects[i][j]->getNeuron();
                            for (int ch = 0; ch < DENSE_LAYER_CHANNEL; ch++) {
                                Neuron *neuron = neuronVect[ch];
                                vector<float> derror = derrors[i];
                                vector<NeuronData *> *ndata = neuron->getData();
                                for (int m = 0; m < derror.size(); m++) {
                                    NeuronData *data = ndata->at(m);
                                    float gradientNeuron =
                                            derror[m] *
                                            getActivationDerivative(neuron->getActivation(), data->getOutput());
                                    data->setGradient(gradientNeuron);
                                }
                            }
                        }
                    }
                } else if (neuronLayer->getLayer()[0][0]->getActivation() == "linear") {
                    vector<vector<float>> derrors = derivativeError(expectedOutputs, actualOutput,
                                                                    neuronLayer->getLayer()[0][0]->getActivation());

                    const int rowStartIndex = neuronLayer->getRowStartIndex();
                    const int rowEndIndex = neuronLayer->getRowEndIndex();
                    const int colStartIndex = neuronLayer->getColStartIndex();
                    const int colEndIndex = neuronLayer->getColEndIndex();
                    vector<float> exp = expectedOutputs.at(0);
                    for (int i = rowStartIndex; i < rowEndIndex; i++) {
                        for (int j = colStartIndex; j < colEndIndex; j++) {

                            vector<Neuron *> neuronVect = *neuronVects[i][j]->getNeuron();
                            for (int ch = 0; ch < DENSE_LAYER_CHANNEL; ch++) {
                                Neuron *neuron = neuronVect[ch];
                                vector<float> derror = derrors[i];
                                vector<NeuronData *> *ndata = neuron->getData();
                                for (int m = 0; m < derror.size(); m++) {
                                    NeuronData *data = ndata->at(m);
                                    float gradientNeuron =
                                            2 * derror.at(m) * data->getInput();
                                    data->setGradient(gradientNeuron);
                                }
                            }
                        }
                    }
                } else
                    //Input layer gradient computation started.
                if (neuronLayer->getLayer()[0][0]->getActivation() == "softmax") {
                    for (int b = 0; b < BATCH_SIZE; b++) {
                        vector<float> expectedOutput = expectedOutputs[b];
                        const int rowStartIndex = neuronLayer->getRowStartIndex();
                        const int rowEndIndex = neuronLayer->getRowEndIndex();
                        const int colStartIndex = neuronLayer->getColStartIndex();
                        const int colEndIndex = neuronLayer->getColEndIndex();

                        for (int i = rowStartIndex; i < rowEndIndex; i++) {
                            for (int j = colStartIndex; j < colEndIndex; j++) {
                                vector<Neuron *> *neuronVect = neuronVects[i][j]->getNeuron();
                                for (int ch = 0; ch < neuronVect->size(); ch++) {
                                    totalIterations += 1;
                                    Neuron *neuron = neuronVect->at(ch);
                                    vector<NeuronData *> *nd = neuron->getData();
                                    NeuronData *data = nd->at(b);
                                    if (flag == 1) {
                                        data->setGradient((data->getOutput() - expectedOutput[ch]));
                                    } else {
                                        if (neuron->getActivation().compare("softmax") == 0) {
                                            float gradient = data->getOutput() - expectedOutput[i + j];
//                                    cout<<"Softmax gradient is "<<gradient<<","<<"Output is "<<data->getOutput()<<endl;
                                            data->setGradient(gradient);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

                ////Iterate through other Layers
//        vector<NeuronLayer *> *neuronLayers = superOutputLayer->getNeuronLayer();
                NeuronLayer *layer = neuronLayer;
//        for (NeuronLayer *layer:*neuronLayers) {
                if (layer->isOutputLayer() == false) {
                    const int rowStartIndex = layer->getRowStartIndex();
                    const int rowEndIndex = layer->getRowEndIndex();
                    const int colStartIndex = layer->getColStartIndex();
                    const int colEndIndex = layer->getColEndIndex();

                    NeuronVect ***neuronVect = layer->getLayer();
                    for (int i = rowStartIndex; i < rowEndIndex; i++) {
                        for (int j = colStartIndex; j < colEndIndex; j++) {
                            vector<Neuron *> *neuronVects = neuronVect[i][j]->getNeuron();
                            for (int ch = 0; ch < neuronVects->size(); ch++) {
                                Neuron *neuron = neuronVects->at(ch);
                                vector<Link *> *frontLinks = neuron->getFrontLinks();

                                for (int b = 0; b < BATCH_SIZE; b++) {
                                    float gradient = 0;
                                    //compute gradient wrt weight

                                    for (Link *link :*frontLinks) {
                                        vector<NeuronData *> *neuronData1 = link->getFromNeuron()->getData();
                                        vector<NeuronData *> *neuronData2 = link->getToNeuron()->getData();
                                        float out = neuronData1->at(b)->getOutput();
                                        float grad = neuronData2->at(b)->getGradient();
                                        gradient = out * grad;
                                        vector<float> gradientVect = link->getWeight()->getGradientVect();
                                        gradientVect.push_back(gradient);
                                        link->getWeight()->setGradientVect(gradientVect);
                                    }

                                    //compute gradient wrt input
                                    gradient = 0;
                                    float neuronOutput = neuron->getData()->at(b)->getOutput();
                                    float derivative = getActivationDerivative(neuron->getActivation(), neuronOutput);
                                    for (Link *link :*frontLinks) {
                                        float linkWeight = link->getWeight()->getWeight();
                                        vector<NeuronData *> *data = link->getToNeuron()->getData();
                                        float nextNeuronGradient = data->at(b)->getGradient();
//                                        gradient += data->at(b)->getGradient();
                                        gradient += derivative * nextNeuronGradient * linkWeight;
                                    }
                                    NeuronData *data = neuron->getData()->at(b);
                                    data->setGradient(gradient);
                                }
                            }
                        }
                    }
                }
        }
        ////End of Iterate through other Layers
        superOutputLayer = superOutputLayer->getPrevSuperLayer();
    }
    return totalError;
};


void optmizeBias(SuperNeuronLayer* superNeuronLayer,float learningRate)
{
    while (superNeuronLayer!=NULL) {
        vector<NeuronLayer*>*nextLayerNeurons=superNeuronLayer->getNeuronLayer();
        for(NeuronLayer*layer:*nextLayerNeurons) {
            vector<Filter *> *filtersVect = layer->getFilterVect();
            if (filtersVect != NULL) {
                for (int m = 0; m < filtersVect->size(); m++) {
                    Filter *filter = filtersVect->at(m);
                    if (filter->getBiasNeuron() == NULL) {
                        break;
                    }
                    vector<Neuron *> toNeurons = filter->getBiasNeuron()->getToNeurons();
                    float totalGradient = 0;
                    int itr = 0;
                    for (int b = 0; b < BATCH_SIZE; b++) {
                        for (Neuron *neuron:toNeurons) {
                            itr++;
//                    if(neuron->getActivation().compare("linear")==0){
//                        break;
//                    }
                            vector<NeuronData *> *nd = neuron->getData();
                            totalGradient += nd->at(b)->getGradient();
                        }
                    }
                    float beta = 0.9;
                    float dv = filter->getBiasNeuron()->getGradientWithMomentum();
                    dv = beta * dv + (1 - beta) * totalGradient;
//                    cout<<"Gradient with momentum is "<<dv<<endl;
                    filter->getBiasNeuron()->setGradientWithMomentum(dv);
                    float newWt = filter->getBiasNeuron()->getWeight() - learningRate * dv;
                    filter->getBiasNeuron()->setWeight(newWt);
                }
            }
//                layer = layer->getNextLayer();
//                if (layer->getMaxPoolLayer() != NULL) {
//                    layer = layer->getMaxPoolLayer();
//                }
//            }
        }
        superNeuronLayer=superNeuronLayer->getNextSuperLayer();
    }
}



void optimizeBiasLinear(NeuronLayer* layer,float learningRate)
{
    vector<BiasNeuron*>* biasNeuronVect=layer->getBiasNeuronVect();

    for(int m=0;m<biasNeuronVect->size();m++)
    {
        BiasNeuron* biasNeuron=biasNeuronVect->at(m);
        vector<Neuron*> toNeurons=biasNeuron->getToNeurons();
        float totalGradient=0;
        int itr=0;
        for(int b=0;b<BATCH_SIZE;b++) {
            for (Neuron *neuron:toNeurons) {
                vector<NeuronData*>* nd=neuron->getData();
                totalGradient += nd->at(b)->getGradient();
            }
        }
        float beta=0.9;
        float dv=biasNeuron->getGradientWithMomentum();
        dv=beta*dv+(1-beta)*totalGradient;
        biasNeuron->setGradientWithMomentum(dv);
        float newWt = biasNeuron->getWeight() - learningRate * dv;
        biasNeuron->setWeight(newWt);

    }
}

bool compareNeuron(Neuron* n1,Neuron* n2){

    vector<NeuronData*>*nd1=n1->getData();
    vector<NeuronData*>*nd2=n2->getData();
//    float s1=nd1->at(0)->getOutput()+nd1->at(1)->getOutput();
//    float s2=nd2->at(0)->getOutput()+nd2->at(1)->getOutput();

    if(nd1->at(0)->getOutput() > nd2->at(0)->getOutput())
//    if(s1 > s2)
    {
        return true;
    } else{
        return false;
    }
}


bool compareNeuronData(NeuronData* n1,NeuronData* n2){

    if(n1->getOutput() > n2->getOutput())
    {
        return true;
    } else{
        return false;
    }
}

void copyNeuron(Neuron* from,Neuron *to)
{

    vector<NeuronData*>* neuronDataVect=from->getData();
    to->setData(neuronDataVect);
    to->setActivation(from->getActivation());
    to->setBiasNeuron(from->getBiasNeuron());
    to->setBackLinks(NULL);
    vector<Link*>* backLinkVect=from->getBackLinks();
    vector<Link*>* backLinkToNeuron=new vector<Link*>();
    for(int i=0;i<backLinkVect->size();i++)
    {
        Link* backLink=backLinkVect->at(i);
        Link* link=new Link();
        link->setVolatileF(true);

        link->setFromNeuron(backLink->getFromNeuron());
        link->setToNeuron(to);
        link->setWeight(backLink->getWeight());
        backLinkToNeuron->push_back(link);

        ///add link to FromNeuron.
        Neuron*fromNeuron=backLink->getFromNeuron();
        vector<Link*>* frontLinks=fromNeuron->getAllFrontLinks();
        frontLinks->push_back(link);
    }

    to->setBackLinks(backLinkToNeuron);
}
void maxPool2D(NeuronLayer *layer)
{
    NeuronLayer* maxPoolLayer=layer->getMaxPoolLayer();
    NeuronVect*** maxLayerNeuronVects= maxPoolLayer->getLayer();
    vector<BiasNeuron*>* biasNeuronVect=layer->getBiasNeuronVect();
    //remove toNeurons of BiasNeuron
    vector<vector<Neuron*>*> biasNeuronVects;
    for(int i=0;i<biasNeuronVect->size();i++)
    {
        BiasNeuron* biasNeuron=biasNeuronVect->at(i);
        vector<Neuron*> biasNeuronVect=biasNeuron->getToNeurons();;
        biasNeuronVect.clear();
        biasNeuron->setToNeurons(biasNeuronVect);
        biasNeuronVects.push_back(new vector<Neuron*>());
    }
    //End of remove toNeurons of BiasNeuron
    const int rowStartIndex=maxPoolLayer->getRowStartIndex();
    const int rowEndIndex=maxPoolLayer->getRowEndIndex();
    const int colStartIndex=maxPoolLayer->getColStartIndex();
    const int colEndIndex=maxPoolLayer->getColEndIndex();
    for(int r=rowStartIndex;r<rowEndIndex;r++)
    {
        for(int c=colStartIndex;c<colEndIndex;c++)
        {
            vector<Neuron*>* neuronVect=maxLayerNeuronVects[r][c]->getNeuron();

            for(int ch=0;ch<maxPoolLayer->getChannel();ch++)
            {
//                cout<<r<<","<<c<<","<<ch<<endl;
                Neuron* n=neuronVect->at(ch);
                if(n->isPadded()== false){
                    vector<Neuron*>*maxPoolNeurons= n->getPoolNeuronVect();
                    //sort the neurons.
                    //Prepare the vector of NeuronData ##to handle multibatch
                    vector<NeuronData*>* maxNeuronDataVect=new vector<NeuronData*>();
                    for(int b=0;b<BATCH_SIZE;b++) {
                        vector<NeuronData *> nd;
                        for (int n = 0; n < maxPoolNeurons->size(); n++) {
                            NeuronData* neuronData=maxPoolNeurons->at(n)->getData()->at(b);
                            nd.push_back(neuronData);
                        }
                        NeuronData *maxNeuronData = *max_element(nd.begin(),nd.end(),
                                                                 compareNeuronData);
                        maxNeuronDataVect->push_back(maxNeuronData);
                    }
                    Neuron *maxNeuron = *max_element(maxPoolNeurons->begin(),maxPoolNeurons->end(),compareNeuron);
                    maxNeuron->setData(maxNeuronDataVect);

                    copyNeuron(maxNeuron,neuronVect->at(ch));
                    biasNeuronVects[ch]->push_back(neuronVect->at(ch));
                }
            }
        }
    }

    setBackLinkStatus(layer, true,  false);
    //end of Deactivate
    //Reassociate max pool bias neurons
    for(int i=0;i<biasNeuronVects.size();i++)
    {
        biasNeuronVect->at(i)->setToNeurons(*biasNeuronVects.at(i));
    }
}


void setBackLinkStatus(NeuronLayer *layer,bool getAllLinks, bool status)
{
    NeuronVect*** prevLayerNeuronVects=layer->getLayer();
    const int rowStartIndex=layer->getRowStartIndex();
    const int rowEndIndex=layer->getRowEndIndex();
    const int colStartIndex=layer->getColStartIndex();
    const int colEndIndex=layer->getColEndIndex();

    for(int r=rowStartIndex;r<rowEndIndex;r++)
    {
        for(int c=colStartIndex;c<colEndIndex;c++)
        {
            vector<Neuron*>* neuronVect= prevLayerNeuronVects[r][c]->getNeuron();
            for(int ch=0;ch<layer->getChannel();ch++)
            {
                vector<Link*>* backLinks;
                if(getAllLinks== false)
                {
                    backLinks=neuronVect->at(ch)->getBackLinks();
                }
                else
                {
                    backLinks=neuronVect->at(ch)->getAllBackLinks();
                }

                for(Link* link:*backLinks)
                {
                    link->setAlive(status);
                }
            }

        }

    }
}


void setFrontLinkStatus(NeuronLayer *layer,bool getAllLinks, bool status)
{
    NeuronVect*** prevLayerNeuronVects=layer->getLayer();
    const int rowStartIndex=layer->getRowStartIndex();
    const int rowEndIndex=layer->getRowEndIndex();
    const int colStartIndex=layer->getColStartIndex();
    const int colEndIndex=layer->getColEndIndex();

    for(int r=rowStartIndex;r<rowEndIndex;r++)
    {
        for(int c=colStartIndex;c<colEndIndex;c++)
        {
            vector<Neuron*>* neuronVect= prevLayerNeuronVects[r][c]->getNeuron();
            for(int ch=0;ch<layer->getChannel();ch++)
            {
                vector<Link*> *frontLinks;
                if(getAllLinks== false)
                {
                    frontLinks=neuronVect->at(ch)->getFrontLinks();
                }
                else
                {
                    frontLinks=neuronVect->at(ch)->getAllFrontLinks();
                }

                for(Link* link:*frontLinks)
                {
                    link->setAlive(status);
                }
            }

        }

    }
}




void optmizeErrorDev(SuperNeuronLayer* superInputLayer,float learningRate) {
    SuperNeuronLayer* tempSuperInputLayer=superInputLayer;

    while (superInputLayer!=NULL) {
        vector<NeuronLayer *> *neuronLayers = superInputLayer->getNeuronLayer();
        for (int n=0;n<neuronLayers->size();n++) {
            NeuronLayer *layer=neuronLayers->at(n);
            vector<Filter *> *filterVect = layer->getFilterVect();
            if(filterVect==NULL){

            } else {
                for (int n = 0; n < filterVect->size(); n++) {
                    Filter *filter = filterVect->at(n);
                    WeightVect ***weightVects = filter->getWeightVect();
                    for (int i = 0; i < filter->getRow(); i++) {
                        for (int j = 0; j < filter->getColumn(); j++) {
                            vector<Weight *> *weightVect = weightVects[i][j]->getWeightVect();
                            for (Weight *w:*weightVect) {
                                vector<float> gradientVect = w->getGradientVect();
                                float filterGradient = 0;
                                for (float gradient: gradientVect) {
                                    filterGradient += gradient;
                                }
                                gradientVect.clear();
                                w->setGradientVect(gradientVect);
                                w->setGradient(filterGradient);
                                //Update weights
                                float beta = 0.9;
                                float dv = w->getGradientWithMomentum();
                                dv = beta * dv + (1 - beta) * filterGradient;
                                w->setGradientWithMomentum(dv);
                                float neuronGradient = w->getWeight() - learningRate * dv;
                                w->setWeight(neuronGradient);
                            }

                        }
                    }
                }
            }
//                layer = layer->getNextLayer();

//                if (layer->getMaxPoolLayer() != NULL) {
//                    layer = layer->getMaxPoolLayer();
//                }
//                if (layer->getNextLayer() == NULL) {
//                    break;
//                } else {
//                    filterVect = layer->getFilterVect();
//                }


        }
        superInputLayer=superInputLayer->getNextSuperLayer();
    }
    optmizeBias(tempSuperInputLayer, learningRate);

}


#endif //CNN_NEURONOPS_H
