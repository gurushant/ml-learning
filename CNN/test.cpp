#include <stdlib.h>
#include <iostream>
#include "mysql/include/jdbc/cppconn/connection.h"
#include "mysql/include/jdbc/cppconn/driver.h"
#include "mysql/include/jdbc/cppconn/exception.h"
#include "mysql/include/jdbc/cppconn/resultset.h"
#include "mysql/include/jdbc/cppconn/statement.h"



using namespace sql;
using namespace std;
int main1(void){
    sql::Driver *driver;
    sql::Connection *con;
    sql::Statement *stmt;
    sql::ResultSet *res;


    driver = get_driver_instance();
    con = driver->connect("tcp://127.0.0.1:3306","root","root");
    con->setSchema("bigdatalab");
    stmt = con->createStatement();
    res = stmt->executeQuery("show tables");
    while (res->next()) {
        /* Access column data by alias or column name */
        cout << res->getString("Tables_in_bigdatalab") << endl;
        /* Access column data by numeric offset, 1 is the first column */
        cout << res->getString(1) << endl;
    }
    return 0;
}