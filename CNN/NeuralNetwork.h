//
// Created by user on 23/8/18.
//
using namespace std;
#ifndef CNN_NEURALNETWORK_H
#define CNN_NEURALNETWORK_H
const int BATCH_SIZE=1;
//const int BOUNDINNG_BOX=2;
//const int OUTPUT_NEURONS=CLASSIFICATION_NEURONS+BOUNDINNG_BOX;
const int OUTPUT_NEURONS=2;
const int DENSE_INDEX=0;
const int DENSE_LAYER_CHANNEL=1;
const int PADDING_ROW=1;
const int PADDING_COLUMN=1;
const string BOUNDING_BOX_ACTIVATION="linear";

#endif //CNN_NEURALNETWORK_H

