//
// Created by gurushant on 05/08/18.
//

#ifndef CNN_ACTIVATION_H
#define CNN_ACTIVATION_H

#include "math.h"
#include "iostream"

using namespace std;
//Sigmoid function implementation
float sigmoid(float input)
{
    return 1/(1+exp(-input));
}

//Sigmoid function derivative implementation
float dsigmoid(float input)
{
    return input*(1-input);
}


//tanh function implementation
float sigmoidTanh(float input)
{

    return tanh(input);
}

//tanh function derivative implementation
float dSigmoidTanh(float input)
{
    return 1-pow(input,2);
}

float softmax(float input)
{
//    cout<<"Input value is "<<input<<endl;
    input=exp(input);
//    cout<<"Output value is "<<input<<endl;
    return input;
}
float dsoftmax(float expected,float actual)
{
    return (expected-actual);
}

float relu(float input)
{
    if(input>=0)
    {
        return input;
    } else
        return 0;
}
float drelu(float input)
{
    if(input>=0)
    {
        return 1;
    } else{
        return 0;
    }
}

#endif //CNN_ACTIVATION_H
