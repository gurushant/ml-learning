//
// Created by user on 17/9/18.
//

#ifndef CNN_MAXPOOL_H
#define CNN_MAXPOOL_H

class MaxPool{

public:MaxPool(){}
public:MaxPool(int row,int column,int stride,int channel){
    this->filterRow=row;
    this->filterColumn=column;
    this->channel=channel;
    this->stride=stride;
}
private:int filterRow;
        int filterColumn;
public:
    int getFilterRow() const {
        return filterRow;
    }

    void setFilterRow(int filterRow) {
        MaxPool::filterRow = filterRow;
    }

    int getFilterColumn() const {
        return filterColumn;
    }

    void setFilterColumn(int filterColumn) {
        MaxPool::filterColumn = filterColumn;
    }

private:
    int stride;
        int channel;
public:


    int getStride() const {
        return stride;
    }

    void setStride(int stride) {
        MaxPool::stride = stride;
    }

    int getChannel() const {
        return channel;
    }

    void setChannel(int channel) {
        MaxPool::channel = channel;
    }
};
#endif //CNN_MAXPOOL_H
