//
// Created by user on 15/11/18.
//

#ifndef CNN_SUPERNEURALLAYER_H
#define CNN_SUPERNEURALLAYER_H

#include "vector"
#include "NeuronLayer.h"

using namespace std;

class SuperNeuronLayer{
private:vector<NeuronLayer*> *neuronLayer;
public:
    int getId() const {
        return id;
    }

    void setId(int id) {
        SuperNeuronLayer::id = id;
    }

private:
    int id;
    static int statId;


public:SuperNeuronLayer(){
        statId++;
        setId(statId);
        neuronLayer=new vector<NeuronLayer*>();
}
public:
    SuperNeuronLayer *getNextSuperLayer() const {
        return nextSuperLayer;
    }

    void setNextSuperLayer(SuperNeuronLayer *nextSuperLayer) {
        SuperNeuronLayer::nextSuperLayer = nextSuperLayer;
    }

    SuperNeuronLayer *getPrevSuperLayer() const {
        return prevSuperLayer;
    }

    void setPrevSuperLayer(SuperNeuronLayer *prevSuperLayer) {
        SuperNeuronLayer::prevSuperLayer = prevSuperLayer;
    }

private:
    SuperNeuronLayer* nextSuperLayer;
    SuperNeuronLayer* prevSuperLayer;

public:
    vector<NeuronLayer *> *getNeuronLayer() const {
        return neuronLayer;
    }

    void setNeuronLayer(vector<NeuronLayer *> *neuronLayer) {
        SuperNeuronLayer::neuronLayer = neuronLayer;
    }

    void addNeuronLayer(NeuronLayer *neuronLayer) {
        getNeuronLayer()->push_back(neuronLayer);
    }
};

int SuperNeuronLayer::statId=0;
#endif //CNN_SUPERNEURALLAYER_H
