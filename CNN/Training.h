#include "Data.h"
#include "vector"
//
// Created by user on 19/10/18.
//
using namespace std;
#ifndef CNN_TRAINING_H
#define CNN_TRAINING_H
class Training{
private:vector<Data> data;
    vector<float> boundingBoxVect;
public:
    const vector<float> &getBoundingBoxVect() const {
        return boundingBoxVect;
    }

    void setBoundingBoxVect(const vector<float> &boundingBoxVect) {
        Training::boundingBoxVect = boundingBoxVect;
    }

private:
    int expectedOutput;
public:
    const vector<Data> &getData() const {
        return data;
    }

    void setData(const vector<Data> &data) {
        Training::data = data;
    }

    int getExpectedOutput() const {
        return expectedOutput;
    }

    void setExpectedOutput(int expectedOutput) {
        Training::expectedOutput = expectedOutput;
    }
};

#endif //CNN_TRAINING_H
