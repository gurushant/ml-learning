//
// Created by user on 20/10/18.
//
#include "vector"
#include "Data.h"
#ifndef CNN_TESTDATA_H
#define CNN_TESTDATA_H
using namespace std;
class TestData{
    vector<Data> testData;
public:
    const vector<Data> &getTestData() const {
        return testData;
    }

    void setTestData(const vector<Data> &testData) {
        TestData::testData = testData;
    }
};
#endif //CNN_TESTDATA_H
