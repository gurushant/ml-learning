//
// Created by gurushant on 05/08/18.
//

#ifndef CNN_WEIGHT_H
#define CNN_WEIGHT_H

#include "vector"

using namespace std;
class Weight
{
public:
    Weight(){
        statId++;
        setId(statId);
    }
    float getWeight() const {
        return weight;
    }

    void setWeight(float weight) {
        Weight::weight = weight;
    }

    const vector<float> &getGradientVect() const {
        return gradientVect;
    }

    void setGradientVect(const vector<float> &gradientVect) {
        Weight::gradientVect = gradientVect;
    }

private:float weight;
    vector<float> gradientVect;
    float gradient;
public:
    float getGradient() const;

    void setGradient(float gradient);

public:
    int getId() const {
        return id;
    }

    void setId(int id) {
        Weight::id = id;
    }

private:
    static int statId;
    int id;
public:
    float getGradientWithMomentum() const {
        return gradientWithMomentum;
    }

    void setGradientWithMomentum(float gradientWithMomentum) {
        Weight::gradientWithMomentum = gradientWithMomentum;
    }

private:
    float gradientWithMomentum=0;
};
int Weight::statId=0;

float Weight::getGradient() const {
    return gradient;
}

void Weight::setGradient(float gradient) {
    Weight::gradient = gradient;
}

#endif //CNN_WEIGHT_H