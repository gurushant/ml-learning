//
// Created by gurushant on 05/08/18.
//

#ifndef CNN_NEURONUTILS_H
#define CNN_NEURONUTILS_H

#include "Neuron.h"
#include "set"
#include "NeuronLayer.h"
#include "MaxPool.h"
#include "NeuralNetwork.h"
#include "SuperNeuronLayer.h"

vector<Neuron*> fetchBackwardLinks(vector<Neuron*> backVect)
{
    set<Neuron*>backNeuronVect;
    for(int i=0;i<backVect.size();i++)
    {
        Neuron* neuron=backVect[i];
        vector<Link*> backLinkVect=*neuron->getBackLinks();
        for(int j=0;j<backLinkVect.size();j++)
        {
            backNeuronVect.insert(backLinkVect[j]->getFromNeuron());
        }
    }
    return vector<Neuron*>(backNeuronVect.begin(),backNeuronVect.end());
}

vector<Neuron*> fetchForwardLinks(vector<Neuron*> forVect)
{
    set<Neuron*>forNeuronVect;
    for(int i=0;i<forVect.size();i++)
    {
        Neuron* neuron=forVect[i];
        vector<Link*>forLinkVect=*neuron->getFrontLinks();
        for(int j=0;j<forLinkVect.size();j++)
        {
            forNeuronVect.insert(forLinkVect[j]->getToNeuron());
        }
    }
    return vector<Neuron*>(forNeuronVect.begin(),forNeuronVect.end());
}

int layerIndex=0;
vector<Filter*>* createFilter(int row,int column,int noOfFilters,NeuronLayer* layer)
{
    int depth=layer->getChannel();
    cout<<"++++++++++++++++++++++++++++++++++++++"<<endl;
    vector<Filter*>* filterVect=new  vector<Filter*>();
    for(int i=0;i<noOfFilters;i++) {
        Filter *filter = new Filter();
        filter->setColumn(column);
        filter->setRow(row);
        filter->setChannel(depth);
        filter->createWeight();
        filterVect->push_back(filter);
    }
//    layer->setFilterVect(filterVect);
//    if(layer->getMaxPoolLayer()!=NULL)
//    {
//        layer->getMaxPoolLayer()->setFilterVect(filterVect);
//    }
    return filterVect;
}

NeuronLayer* createNeuronLayer(NeuronVect *** inputLayer,int row,int column,int channel,SuperNeuronLayer* superNeuronLayer)
{

    NeuronLayer *layer1=new NeuronLayer();
    layer1->setLayer(inputLayer);
    layer1->setRow(row);
    layer1->setColumn(column);
    layer1->setChannel(channel);
    layer1->setRowStartIndex(PADDING_ROW);
    layer1->setRowEndIndex(PADDING_ROW+row);
    layer1->setColStartIndex(PADDING_COLUMN);
    layer1->setColEndIndex(PADDING_COLUMN+column);
    layerIndex++;
    superNeuronLayer->addNeuronLayer(layer1);
    return layer1;
}


NeuronLayer* createNeuronLayerForDense(NeuronVect *** inputLayer,int row)
{
    int column=1;
    int channel=1;
    NeuronLayer *layer1=new NeuronLayer();
    layer1->setLayer(inputLayer);
    layer1->setRow(row);
    layer1->setColumn(column);
    layer1->setChannel(channel);
    layer1->setRowStartIndex(0);
    layer1->setRowEndIndex(row);
    layer1->setColStartIndex(0);
    layer1->setColEndIndex(1);

    layerIndex++;
    return layer1;
}


vector<Filter*>* createFCFilter(int row,int column,NeuronLayer* layer)
{
    int noOfFilters=1;
    int depth=layer->getChannel();
    vector<Filter*>* filterVect=new  vector<Filter*>();
    for(int i=0;i<noOfFilters;i++) {
        Filter *filter = new Filter();
        filter->setColumn(column);
        filter->setRow(row);
        filter->setChannel(depth);
        filter->createWeight();
        filterVect->push_back(filter);
    }
//    layer->setFilterVect(filterVect);
//    if(layer->getMaxPoolLayer()!=NULL)
//    {
//        layer->getMaxPoolLayer()->setFilterVect(filterVect);
//    }
    return filterVect;
}


float vectorSum(vector<float> vect)
{
    float total=0;
    for(float val : vect)
    {
        total+=val;
    }
    return total;
}
Link* createLink()
{
    Link *link=new Link();
    return link;
}

float getRandom()
{
    float LOW=-1;
    float HIGH=1;
    float randVal=(LOW+static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HIGH-LOW))));
    return randVal;
}


int neuronIndex=0;
NeuronVect*** createNeurons(int row,int column,int channel, string activation,string note)
{
    row=row+2*PADDING_ROW;
    column=column+2*PADDING_COLUMN;


    cout<<"-----------------------------------------"<<endl;
    NeuronVect ***inputLayer=new NeuronVect**[row];
    for(int i=0;i<row;i++)
    {
        inputLayer[i]=new NeuronVect*[column];
    }
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<column;j++) {
            inputLayer[i][j]=new NeuronVect();
            if(!activation.empty())
            {
                inputLayer[i][j]->setActivation(activation);
            }
            vector<Neuron*>* neuronVect=inputLayer[i][j]->getNeuron();
            //create vector for number of channels.
            for(int c=0;c<channel;c++)
            {
                Neuron* neuron=new Neuron();
                neuron->setActivation(activation);
                neuronVect->push_back(neuron);
                neuron->setNote(note);
                ///Prepare NeuronData vector
                vector<NeuronData*>*ndVect=new vector<NeuronData*>();
                for(int b=0;b<BATCH_SIZE;b++)
                {
                    NeuronData* nd=new NeuronData(1);
                    ndVect->push_back(nd);
                }
                neuron->setData(ndVect);
            }
            //End of create vector for number of channels.
            neuronIndex++;
        }
    }

    //Start padding for upper and lower part of neurons
    ///Mark padded neurons set flag from start
    for(int i=0;i<PADDING_ROW;i++)
    {
        for(int j=0;j<column;j++) {
            vector<Neuron *> *neuronVect = inputLayer[i][j]->getNeuron();
            for (int s = 0; s < neuronVect->size(); s++) {
                Neuron *neuron = neuronVect->at(s);
                neuron->setPadded(true);

            }
        }
    }

    ///Mark padded neurons set flag from end
    const int rowCond=row-PADDING_ROW-1;

    for(int i=(row-1);i> rowCond;i--)
    {
        for(int j=0;j<column;j++) {
            vector<Neuron *> *neuronVect = inputLayer[i][j]->getNeuron();
            for (int s = 0; s < neuronVect->size(); s++) {
                Neuron *neuron = neuronVect->at(s);
                neuron->setPadded(true);

            }
        }
    }
    //End of Padding
    //End of padding for upper and lower part of neurons


    //Start padding for left and right part of neurons
    ///Mark padded neurons set flag from start
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<PADDING_COLUMN;j++) {
            vector<Neuron *> *neuronVect = inputLayer[i][j]->getNeuron();
            for (int s = 0; s < neuronVect->size(); s++) {
                Neuron *neuron = neuronVect->at(s);
                neuron->setPadded(true);

            }
        }
    }

    ///Mark padded neurons set flag from end
     int colCond=column-PADDING_COLUMN-1;

    for(int i=0;i<row;i++)
    {
        for(int j=(column-1);j>colCond;j--) {
            vector<Neuron *> *neuronVect = inputLayer[i][j]->getNeuron();
            for (int s = 0; s < neuronVect->size(); s++) {
                Neuron *neuron = neuronVect->at(s);
                neuron->setPadded(true);

            }
        }
    }
    //End of Padding
    //End of padding for upper and lower part of neurons


    return inputLayer;
}

NeuronVect*** createDenseNeurons(int row, string activation,string note)
{

    int column=1;
    int channel=1;
    cout<<"-----------------------------------------"<<endl;
    NeuronVect ***inputLayer=new NeuronVect**[row];
    for(int i=0;i<row;i++)
    {
        inputLayer[i]=new NeuronVect*[column];
    }
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<column;j++) {
            inputLayer[i][j]=new NeuronVect();
            if(!activation.empty())
            {
                inputLayer[i][j]->setActivation(activation);
            }
            vector<Neuron*>* neuronVect=inputLayer[i][j]->getNeuron();
            //create vector for number of channels.
            for(int c=0;c<channel;c++)
            {
                Neuron* neuron=new Neuron();
                neuron->setActivation(activation);
                neuronVect->push_back(neuron);
                neuron->setNote(note);

                ///Prepare NeuronData obj
                vector<NeuronData*>*ndVect=new vector<NeuronData*>();
                for(int b=0;b<BATCH_SIZE;b++)
                {
                    NeuronData* nd=new NeuronData(1);
                    ndVect->push_back(nd);
                }
                neuron->setData(ndVect);
            }
            //End of create vector for number of channels.
            neuronIndex++;
        }
    }
    ///Set Linear Regression Loss function ///
//    for(int i=row-BOUNDINNG_BOX;i<row;i++)
//    {
//        for(int j=0;j<column;j++) {
//            vector<Neuron*>* neurons=inputLayer[i][j]->getNeuron();
//            for(int i=0;i<neurons->size();i++)
//            {
//                neurons->at(i)->setActivation(BOUNDING_BOX_ACTIVATION);
//            }
//        }
//    }



    return inputLayer;
}


NeuronVect*** createNeuronEmpty(int row,int column, string activation)
{
    NeuronVect ***inputLayer=new NeuronVect**[row];
    for(int i=0;i<row;i++)
    {
        inputLayer[i]=new NeuronVect*[column];
    }
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<column;j++) {
            NeuronVect* e=new NeuronVect();
            inputLayer[i][j] =e ;
            if(!activation.empty())
            {
                inputLayer[i][j]->setActivation(activation);
            }
            neuronIndex++;
        }
    }
    return inputLayer;
}

void setMaxPool(MaxPool* maxPool,NeuronLayer* layer)
{
    int rows=layer->getRow();
    int cols=layer->getColumn();
    int ch=layer->getChannel();
//    int nextLayerRows=rows/maxPool->getFilterRow();
//    int nextLayerCols=cols/maxPool->getFilterColumn();
    int nextLayerRows = ((rows - maxPool->getFilterRow())/ maxPool->getStride()) + 1;
    int nextLayerCols = ((cols- maxPool->getFilterColumn())/ maxPool->getStride()) + 1;

    NeuronVect*** currentNeuronVect=layer->getLayer();
    NeuronVect*** maxNeuronVects=createNeurons(nextLayerRows,nextLayerCols,ch,
            layer->getLayer()[0][0]->getActivation(),"maxPoolLayer");

    NeuronLayer* maxLayer=createNeuronLayer(maxNeuronVects,nextLayerRows,nextLayerCols,ch,NULL);
    int fw=maxPool->getFilterRow();
    int fh=maxPool->getFilterColumn();
    int stride=maxPool->getStride();

    int z=0;
    for(int ch=0;ch<layer->getChannel();ch++) {
        const int rowEndIndex=layer->getRowEndIndex();
        const int colEndIndex=layer->getColEndIndex();
        for (int r = layer->getRowStartIndex(),i=maxLayer->getRowStartIndex(); (r+fw) <= rowEndIndex; r+=stride,i++) {
            for (int c = layer->getColStartIndex(),j=maxLayer->getColStartIndex(); (c+fh) <= colEndIndex; c+=stride,j++) {
                vector<Neuron*>* tempNeuronVect=new vector<Neuron*>();
//                cout<<r<<","<<c<<endl;
                for(int rr=r;rr<(r+fw);rr++)
                {
                    for(int cc=c;cc<(c+fh);cc++)
                    {
                        vector<Neuron*>* neuronVect=currentNeuronVect[rr][cc]->getNeuron();
                        tempNeuronVect->push_back(neuronVect->at(ch));
                    }
                }
                //Set max neuron vect
                vector<Neuron*>* maxNeuronVect= maxNeuronVects[i][j]->getNeuron();
                Neuron* neuron=maxNeuronVect->at(ch);
                neuron->setPoolNeuronVect(tempNeuronVect);

            }
        }
    }
//    maxLayer->setNextLayer(layer->getNextLayer());
//    maxLayer->setFilterVect(layer->getFilterVect());
//    maxLayer->setPrevLayer(layer->getPrevLayer());
//    layer->setNextLayer(NULL);
    layer->setMaxPoolLayer(maxLayer);
}
#endif //CNN_NEURONUTILS_H
