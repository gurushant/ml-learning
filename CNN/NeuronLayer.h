//
// Created by gurushant on 05/08/18.
//

#ifndef CNN_NEURONLAYER_H
#define CNN_NEURONLAYER_H

#include "Neuron.h"
#include "Filter.h"
#include <iostream>
#include "list"
#include "vector"
#include "cmath"
#include "set"
#include <stdlib.h>
#include <iostream>
#include "MaxPool.h"
#include "Output.h"

using namespace std;
class NeuronLayer
{
public:
    bool isOutputLayer() const {
        return outputLayer;
    }

    void setOutputLayer(bool outputLayer) {
        NeuronLayer::outputLayer = outputLayer;
    }

private:bool outputLayer= false;
    Output* expectedOutput;
public:
    Output *getExpectedOutput() const {
        return expectedOutput;
    }

    void setExpectedOutput(Output *expectedOutput) {
        NeuronLayer::expectedOutput = expectedOutput;
    }

public:NeuronLayer(){
        statId++;
        setId(statId);
        biasNeuronVect=new vector<BiasNeuron*>();
        nextNeuronLayerVect=new vector<NeuronLayer*>();
        prevNeuronLayerVect=new vector<NeuronLayer*>();

    }
public:
    int getRow() const {
        return row;
    }

    void setRow(int row) {
        NeuronLayer::row = row;
    }

    int getColumn() const {
        return column;
    }

    void setColumn(int column) {
        NeuronLayer::column = column;
    }

private:
    NeuronVect ***layer;
    vector<BiasNeuron*>* biasNeuronVect;
public:
    vector<BiasNeuron *> *getBiasNeuronVect() const {
        return biasNeuronVect;
    }

    void setBiasNeuronVect(vector<BiasNeuron *> *biasNeuronVect) {
        NeuronLayer::biasNeuronVect = biasNeuronVect;
    }

public:
    NeuronVect ***getLayer() const {
        return layer;
    }

    void setLayer(NeuronVect ***layer) {
        NeuronLayer::layer = layer;
    }

private:
    int row;
    int column;
    int rowStartIndex=0;
    string note;
public:
    const string &getNote() const {
        return note;
    }

    void setNote(const string &note) {
        NeuronLayer::note = note;
    }

public:
    int getRowStartIndex() const {
        return rowStartIndex;
    }

    void setRowStartIndex(int rowStartIndex) {
        NeuronLayer::rowStartIndex = rowStartIndex;
    }

    int getRowEndIndex() const {
        return rowEndIndex;
    }

    void setRowEndIndex(int rowEndIndex) {
        NeuronLayer::rowEndIndex = rowEndIndex;
    }

    int getColStartIndex() const {
        return colStartIndex;
    }

    void setColStartIndex(int colStartIndex) {
        NeuronLayer::colStartIndex = colStartIndex;
    }

    int getColEndIndex() const {
        return colEndIndex;
    }

    void setColEndIndex(int colEndIndex) {
        NeuronLayer::colEndIndex = colEndIndex;
    }

private:
    int rowEndIndex=0;
    int colStartIndex=0;
    int colEndIndex=0;
    vector<NeuronLayer*>* nextNeuronLayerVect;
    vector<NeuronLayer*>* prevNeuronLayerVect;
public:
    vector<NeuronLayer *> *getNextNeuronLayerVect() const;

    void setNextNeuronLayerVect(vector<NeuronLayer *> *nextNeuronLayerVect);

    vector<NeuronLayer *> *getPrevNeuronLayerVect() const;

    void setPrevNeuronLayerVect(vector<NeuronLayer *> *prevNeuronLayerVect);

private:
    NeuronLayer* maxPoolLayer=NULL;
    MaxPool* maxPool=NULL;
public:
    MaxPool *getMaxPool() const {
        return maxPool;
    }

    void setMaxPool(MaxPool *maxPool) {
        NeuronLayer::maxPool = maxPool;
    }

public:
    NeuronLayer *getMaxPoolLayer() const {
        return maxPoolLayer;
    }

    void setMaxPoolLayer(NeuronLayer *maxPoolLayer) {
        NeuronLayer::maxPoolLayer = maxPoolLayer;
    }

public:
    int getId() const {
        return id;
    }

    void setId(int id) {
        NeuronLayer::id = id;
    }

private:
    static int statId;
    int id;

private:
    vector<Filter*>* filterVect;
public:
    vector<Filter *> *getFilterVect() const {
        return filterVect;
    }

    void setFilterVect(vector<Filter *> *filterVect) {
        NeuronLayer::filterVect = filterVect;
    }

public:
    int getChannel() const {
        return channel;
    }

    void setChannel(int channel) {
        NeuronLayer::channel = channel;
    }

private:
    int channel=1;

};
int NeuronLayer::statId=0;

vector<NeuronLayer *> *NeuronLayer::getNextNeuronLayerVect() const {
    return nextNeuronLayerVect;
}

void NeuronLayer::setNextNeuronLayerVect(vector<NeuronLayer *> *nextNeuronLayerVect) {
    NeuronLayer::nextNeuronLayerVect = nextNeuronLayerVect;
}

vector<NeuronLayer *> *NeuronLayer::getPrevNeuronLayerVect() const {
    return prevNeuronLayerVect;
}

void NeuronLayer::setPrevNeuronLayerVect(vector<NeuronLayer *> *prevNeuronLayerVect) {
    NeuronLayer::prevNeuronLayerVect = prevNeuronLayerVect;
}

#endif //CNN_NEURONLAYER_H