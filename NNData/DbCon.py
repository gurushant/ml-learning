import MySQLdb

db_con=None
def get_db_con():
    global db_con
    if db_con is None:
        db_con = MySQLdb.Connection(
            host="localhost",
            user="root",
            passwd="root",
            db="ml"
        )
    return db_con


