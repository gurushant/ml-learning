import DbCon
import json

db_con=DbCon.get_db_con()



def fetchData():
    super_layer_id_list=fetch_super_layer_id()
    neural_network_json=[]
    for super_layer_id in super_layer_id_list:
        super_layer_json = {"superNeuronLayerId":super_layer_id}
        # super_layer_json.append({"superNeuronLayerId":super_layer_id})
        layer_id_list=fetch_layer_id(super_layer_id)
        layer_json=None
        for layer_id in layer_id_list:
            layer_json={"layerId":layer_id}
            neurons_json=[]
            channel_id_list=fetch_channel_id(super_layer_id,layer_id)
            for channel_id in channel_id_list:
                neuron_id_list=fetch_neuron_id(super_layer_id,layer_id,channel_id)
                neuron_json=[]
                for neuron_id in neuron_id_list:
                    front_neuron_id_list=fetch_links(neuron_id,True)
                    back_neuron_id_list=fetch_links(neuron_id,False)
                    neuron_data_id_list=fetch_neuron_data(neuron_id)
                    neuron_json.append({"id":neuron_id,"frontNeurons":front_neuron_id_list,
                                        "backNeurons":back_neuron_id_list,
                                        "neuronData":neuron_data_id_list})
                neurons_json.append({channel_id:neuron_json})
        layer_json["neurons"]=neurons_json
        super_layer_json["layers"]=layer_json
        neural_network_json.append(super_layer_json)


    neural_network_json=json.dumps(neural_network_json)
    print neural_network_json

def fetch_super_layer_id():
    db_con = DbCon.get_db_con()
    cursor = db_con.cursor()
    cursor.execute("select distinct superLayerId from neuron")
    recs = cursor.fetchall()
    cursor.close()
    super_layer_id_list=[]
    for rec in recs:
        super_layer_id_list.append(rec[0])
    return super_layer_id_list


def fetch_layer_id(superLayerId):
    db_con = DbCon.get_db_con()
    cursor = db_con.cursor()
    cursor.execute("select distinct layerId from neuron where superLayerId=%s",(superLayerId,))
    recs = cursor.fetchall()
    cursor.close()
    layer_id_list=[]
    for rec in recs:
        layer_id_list.append(rec[0])
    return layer_id_list

def fetch_channel_id(superLayerId,layerId):
    db_con = DbCon.get_db_con()
    cursor = db_con.cursor()
    cursor.execute("select distinct channelIndex from neuron where superLayerId=%s and layerId=%s",(superLayerId,layerId,))
    recs = cursor.fetchall()
    cursor.close()
    channel_id_list=[]
    for rec in recs:
        channel_id_list.append(rec[0])
    return channel_id_list


def fetch_neuron_id(superLayerId,layerId,channelId):
    db_con = DbCon.get_db_con()
    cursor = db_con.cursor()
    cursor.execute("select neuronId from neuron where superLayerId=%s and layerId=%s and channelIndex=%s order by neuronId"
                   ,(superLayerId,layerId,channelId,))
    recs = cursor.fetchall()
    cursor.close()
    neuron_id_list=[]
    for rec in recs:
        neuron_id_list.append(rec[0])
    return neuron_id_list


def fetch_links(fromNeuronId,isForward):
    db_con = DbCon.get_db_con()
    cursor = db_con.cursor()
    cursor.execute("select linkId from link where fromNeuronId=%s and isForward=%s"
                   ,(fromNeuronId,isForward,))
    recs = cursor.fetchall()
    cursor.close()
    link_id_list=[]
    for rec in recs:
        link_id_list.append(rec[0])
    return link_id_list


def fetch_neuron_data(neuronId):
    db_con = DbCon.get_db_con()
    cursor = db_con.cursor()
    cursor.execute("select input,output,neuronDataId,activation from neuronData where neuronId=%s"
                   ,(neuronId,))
    recs = cursor.fetchall()
    cursor.close()
    nd_id_list=[]
    for rec in recs:
        nd_id_list.append({"input":rec[0],"output":rec[1],"neuronDataId":rec[2],"activation":rec[3]})
    return nd_id_list




fetchData()