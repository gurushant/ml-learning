-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24-0ubuntu0.16.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ml
CREATE DATABASE IF NOT EXISTS `ml` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ml`;

-- Dumping structure for table ml.link
CREATE TABLE IF NOT EXISTS `link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weightId` int(11) NOT NULL,
  `linkId` varchar(32) DEFAULT NULL,
  `fromNeuronId` int(11) NOT NULL,
  `toNeuronId` int(11) NOT NULL,
  `gradient` varchar(32) DEFAULT NULL,
  `isForward` tinyint(4) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1730 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table ml.neuron
CREATE TABLE IF NOT EXISTS `neuron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `superLayerId` int(11) NOT NULL,
  `layerId` int(11) NOT NULL,
  `neuronId` int(11) NOT NULL,
  `channelIndex` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=665 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table ml.neuronData
CREATE TABLE IF NOT EXISTS `neuronData` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `neuronId` int(11) NOT NULL,
  `neuronDataId` int(11) NOT NULL,
  `input` varchar(32) DEFAULT NULL,
  `output` varchar(32) DEFAULT NULL,
  `activation` varchar(32) DEFAULT NULL,
  `gradient` varchar(32) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=631 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table ml.weight
CREATE TABLE IF NOT EXISTS `weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weightId` int(11) NOT NULL,
  `weightValue` double DEFAULT NULL,
  `gradient` varchar(32) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

